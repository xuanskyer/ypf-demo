<?php
/**
 * Desc: 配置管理服务类
 * Created by PhpStorm.
 * User: xuanskyer | <furthestworld@icloud.com>
 * Date: 2015/9/14 11:11
 */

namespace Service\Setting;
class MemberSettingService extends \Service\Service{

    const LOGIN_HOME_SETTING    =   'login_home_domain';
    public $ret;

    public $status_switch = array(
        'on'    =>  1,
        'off'   =>  0
    );
    public function __construct(){
        parent::__construct();
        $this->ret = array('status' => 0, 'info' => '操作失败');
        $this->initModels();
    }


    public function initModels(){
        $this->models = new \stdClass();
        $this->models->view_admin_member_setting                        = new \Model\ViewAdminMemberSetting();
        $this->models->admin_member_setting                        = new \Model\AdminMemberSetting();
        $this->models->admin_setting                        = new \Model\AdminSetting();
        $this->models->admin_setting_group                        = new \Model\AdminSettingGroup();
    }

    public function getMemberSettingList($params = array()){
        $result = array(
            'count'     =>  0,
            'data'      => array()
        );
        extract($params);
        empty($where) && $where = array();
        empty($offset) && $offset = 0;
        empty($limit) && $limit = 20;
        empty($order) && $order = array();
        $result['count'] = $this->models->view_admin_member_setting->getListCountByWhere($where);
        $result['data'] = $this->models->view_admin_member_setting->getListByWhere($where, $offset, $limit, $order);
        return $result;
    }

    /**
     * 保存用户配置
     * @param array $params
     * @return array
     */
    public function saveMemberSetting($params = array()){
        if(!isset($params['id'])){
            return array('status' => 0, 'info' => '未指定配置ID');
        }
        if(!isset($params['setting_value'])){
            return array('status' => 0, 'info' => '未设置配置的值');
        }
        $data['setting_value'] = $params['setting_value'];
        $res = $this->models->admin_member_setting->updateById($params['id'], $data);
        if($res){
            $this->ret = array('status' => 1, 'info' => '修改配置成功');
        }else{
            $this->ret = array('status' => 0, 'info' => '修改配置失败');
        }
        return $this->ret;
    }

    /**
     * 删除用户配置
     * @param int $id
     * @return array
     */
    public function deleteMemberSettingById($id = 0){
        if(!isset($id) || empty($id)){
            return array('status' => 0, 'info' => '未指定配置ID');
        }
        $where['id'] = $id;
        $res = $this->models->admin_member_setting->deleteByWhere($where);
        if($res){
            $this->ret = array('status' => 1, 'info' => '删除配置成功');
        }else{
            $this->ret = array('status' => 0, 'info' => '删除配置失败');
        }
        return $this->ret;
    }

    public function getAllSettingGroup(){

        return  $this->models->admin_setting_group->getListByWhere();

    }

    public function saveMemberSettingGroup($params = array()){
        if(!isset($params['uid'])){
            return array('status' => 0, 'info' => '未指定用户');
        }
        if(!isset($params['group_id'])){
            return array('status' => 0, 'info' => '未指定配置分组');
        }

        $where['member_id'] = $params['uid'];
        $where['group_id'] = $params['group_id'];
        $count = $this->models->admin_member_setting->getListCountByWhere($where);
        if($count){
            return array('status' => 0, 'info' => '用户已添加该配置分组');
        }

        $setting_list = $this->getGroupSettingList($params['group_id']);

        if(empty($setting_list)){
            return array('status' => 0, 'info' => '该配置分组下无配置项');
        }
        $data = array(
            'member_id' =>  $params['uid'],
            'group_id'  =>  $params['group_id'],
            'created_time'  => date('Y-m-d H:i:s', time()),
            'updated_time'  => date('Y-m-d H:i:s', time()),
        );
        foreach((array)$setting_list as $key => $val){
            $data['setting_id'] = $val['id'];
            $data['status'] = 1;
            $res = $this->models->admin_member_setting->add($data);
        }
        if($res){
            $this->ret = array('status' => 1, 'info' => '新增配置分组成功');
        }else{
            $this->ret = array('status' => 0, 'info' => '新增配置分组失败');
        }
        return $this->ret;
    }

    private function getGroupSettingList($group_id = 0){
        if($group_id){
            $where['config_groupid'] = $group_id;
            $list = $this->models->admin_setting->getListByWhere($where);
            return $list;
        }else{
            return array();
        }
    }
}