<?php
namespace Controller\Admin\Cache;
class Index extends \Controller\Admin\Common\Common{

    public $cacheService;
    public $statusService;
    public function __construct(){
        parent::__construct();
        $this->setHeaderFooter();

        $this->cacheService = new \Service\Cache\CacheService();
    }

    public function index(){

        $actions['getCacheList'] = \Url::get_function_url('cache', 'index', 'getCacheList',array(),true);
        $actions['addCache'] = \Url::get_function_url('cache', 'index', 'addCache',array(),true);
        $actions['editCache'] = \Url::get_function_url('cache', 'index', 'editCache',array(),true);
        $actions['updateCache'] = \Url::get_function_url('cache', 'index', 'updateCache',array(),true);
        $actions['deleteCache'] = \Url::get_function_url('cache', 'index', 'deleteCache',array(),true);
        $this->view->assign('actions' ,$actions);
        $this->view->display('Admin/Cache/index.html');
    }

    public function getCacheList(){

        $params = $this->parseJplistStatuses($this->req['statuses']);
        $this->ret = $this->cacheService->getCacheList($params);
        $this->ajaxReturn($this->ret);
    }


    public function addCache(){
        if(IS_POST){
            $this->ret = $this->cacheService->addCache($this->post);
            $log_params['params'] = json_encode($this->post);
            $log_params['message'] = "新增缓存管理：{$this->ret['info']}";
            $this->saveLog($log_params);
        }elseif(IS_GET){

        }
        $this->ajaxReturn($this->ret);
    }

    public function editCache(){
        if(IS_POST){
            $this->ret = $this->cacheService->editCache($this->post);
            $log_params['params'] = json_encode($this->post);
            $log_params['message'] = "编辑缓存管理：{$this->ret['info']}";
            $this->saveLog($log_params);
        }elseif(IS_GET){
            $where['id'] = intval($this->req['id']);
            $cache_info = $this->cacheService->getCacheInfoByWhere($where);
            if(!empty($cache_info)){
                $cache_value = $this->cache_memcached->get($cache_info['cache_key']);
                $cache_info['cache_value'] = $cache_value ? print_r($cache_value,1) : '';
                $this->ret = array('status' => 1, 'info' => '查询成功', 'data' => $cache_info);
            }else{
                $this->ret = array('status' => 0, 'info' => '查询失败');
            }
        }
        $this->ajaxReturn($this->ret);
    }

    public function updateCache(){
        if(IS_POST){
            $this->ret = $this->cacheService->updateCache($this->post);
            $log_params['params'] = json_encode($this->post);
            $log_params['message'] = "更新缓存：{$this->ret['info']}";
            $this->saveLog($log_params);
        }elseif(IS_GET){

        }
        $this->ajaxReturn($this->ret);
    }

    public function deleteCache(){
        $this->ret = $this->cacheService->deleteCacheById($this->post['id']);
        $log_params['params'] = json_encode($this->post);
        $log_params['message'] = "删除缓存管理：{$this->ret['info']}";
        $this->saveLog($log_params);
        $this->ajaxReturn($this->ret);
    }

}
