<?php

date_default_timezone_set('Asia/Shanghai');
define('__ERROR_HANDLE_LEVEL__', E_ALL ^ E_WARNING ^ E_NOTICE);
define("__APP__", dirname(__DIR__)); //Public
define("__VIEW__", __APP__ . '/View/'); //Public
define("__ROOT__", __APP__);
define("__CONF__", __ROOT__ . '/Conf/');
define("__LIBRARY__", __ROOT__ . '/Library');
define("__VIEW_COMMON__", __ROOT__ . '/View/Common');
define("__LOG_PATH__", __ROOT__ . '/Runtime/Logs/');
define("__SESSION_PATH__", "/tmp/session/ypf-demo/");
if ('WIN' !== strtoupper(substr(PHP_OS, 0, 3))) {
    if (!file_exists(__SESSION_PATH__) && !mkdir(__SESSION_PATH__, 755, true)) {
        echo "session目录 " . __SESSION_PATH__ . "不存在,请手动创建~ ";
    }
    session_save_path(__SESSION_PATH__);
}
if (!session_id()) {
    ini_set('session.use_only_cookies', 'On');
    ini_set('session.use_trans_sid', 'Off');
    ini_set('session.cookie_httponly', 'On');
    session_start();
}

/**
 * 框架初始化
 */
$app = new \Ypf\Ypf(
    [
        'root'               => __ROOT__ . '/',     //必填配置项
        'config_path'        => __CONF__,
        'view_path'          => __VIEW__,
        'error_handle_open'  => true,
        'error_handle_level' => __ERROR_HANDLE_LEVEL__,
        'container_setting'  => [
            'config'   => null,     //自定义config对象到容器
            'load'     => null,     //自定义load对象到容器
            'log'      => null,     //自定义log对象到容器
            'request'  => null,     //自定义request对象到容器
            'response' => null,     //自定义response对象到容器
            'view'     => null,     //自定义view对象到容器
            'db'       => null,     //自定义db对象到容器
        ]
    ]
);

$app->run();
