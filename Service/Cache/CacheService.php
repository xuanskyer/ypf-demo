<?php
/**
 * Desc: 缓存管理服务类
 * Created by PhpStorm.
 * User: xuanskyer | <furthestworld@icloud.com>
 * Date: 2015-10-9 16:22:56
 */

namespace Service\Cache;
class CacheService extends \Service\Service{

    CONST TOP_DOMAIN_SUFFIX_GROUPKEY = 'topLevelSufix';       //顶级域名缓存KEY配置分组名
    CONST SENCOND_DOMAIN_SUFFIX_GROUPKEY = 'twoLevelSufix';   //二级域名缓存KEY配置分组名
    CONST DOMAINSUFFIX_CACHE_KEY = 'Suffix-cache-key';        //域名后缀memcache缓存KEY
    public $ret;
    public $status_switch = array(
        'on'    =>  1,
        'off'   =>  0
    );
    public function __construct(){
        parent::__construct();
        $this->ret = array('status' => 0, 'info' => '操作失败');
        $this->initModels();
    }


    public function initModels(){
        $this->models = new \stdClass();
        $this->models->admin_cache                        = new \Model\AdminCache();
        $this->models->admin_setting_group                = new \Model\AdminSettingGroup();
    }



    public function getCacheList($params = array()){
        $result = array(
            'count'     =>  0,
            'data'      => array()
        );
        extract($params);
        empty($where) && $where = array();
        empty($offset) && $offset = 0;
        empty($limit) && $limit = 20;
        empty($order) && $order = array();
        $result['count'] = $this->models->admin_cache->getListCountByWhere($where);
        $cache_list = $this->models->admin_cache->getListByWhere($where, $offset, $limit, $order);
        foreach((array)$cache_list as $key => $val){
            $cache_value = $this->cache_memcached->get($val['cache_key']);
            $cache_list[$key]['cache_value'] = $cache_value ? print_r($cache_value,1) : $cache_value;
        }
        $result['data'] = $cache_list;
        return $result;
    }

    /**
     * 新增缓存管理
     * @param array $params
     * @return array
     */
    public function addCache($params = array()){
        if(!isset($params['cache_key']) || empty($params['cache_key'])){
            return array('status' => 0, 'info' => '缓存KEY不能为空');
        }

        if( $this->checkCacheKeyExisted($params['cache_key'])){
            return array('status' => 0, 'info' => '当前缓存KEY已经存在');
        }

        if(isset($params['cache_value']) && !empty($params['cache_value'])){
            $this->cache_memcached->save($params['cache_key'], $params['cache_value']);
        }
        if(!isset($params['status']) || !isset($this->status_switch[$params['status']])){
            $data['status'] = $this->status_switch['off'];
        }else{
            $data['status'] = $this->status_switch[$params['status']];
        }
        $data['cache_key'] = trim($params['cache_key']);
        !empty($params['description']) && $data['description'] = $params['description'];
        $data['created_time'] = $data['updated_time'] = date('Y-m-d H:i:s', time());
        $res = $this->models->admin_cache->add($data);
        if($res){
            $this->ret = array('status' => 1, 'info' => '新增缓存管理成功');
        }else{
            $this->ret = array('status' => 0, 'info' => '新增缓存管理失败');
        }
        return $this->ret;
    }

    /**
     * 编辑缓存管理
     * @param array $params
     * @return array
     */
    public function editCache($params = array()){
        if(!isset($params['id']) || empty($params['id'])){
            return array('status' => 0, 'info' => '未指定缓存管理ID');
        }
        if(!isset($params['cache_key']) || empty($params['cache_key'])){
            return array('status' => 0, 'info' => '缓存KEY不能为空');
        }elseif($this->checkCacheKeyExisted($params['cache_key'], array('id' => array('not in', $params['id'])))){
            return array('status' => 0, 'info' => '当前缓存KEY已经存在!');
        }
        $cache_value = $this->cache_memcached->get($params['cache_key']);
        $cache_value && $cache_value = json_encode($cache_value);
        if(isset($params['cache_value']) && $cache_value != $params['cache_value']){
            $this->cache_memcached->save($params['cache_key'], $params['cache_value']);
        }
        if(!isset($params['status']) || !isset($this->status_switch[$params['status']])){
            $data['status'] = $this->status_switch['off'];
        }else{
            $data['status'] = $this->status_switch[$params['status']];
        }
        $data['cache_key'] = trim($params['cache_key']);
        !empty($params['description']) && $data['description'] = $params['description'];
        $res = $this->models->admin_cache->updateById($params['id'], $data);
        if($res){
            $this->ret = array('status' => 1, 'info' => '更新缓存管理成功');
        }else{
            $this->ret = array('status' => 0, 'info' => '更新缓存管理失败');
        }
        return $this->ret;
    }

    /**
     * 更新缓存
     * @param array $params
     * @return array
     */
    public function updateCache($params = array()){
        if(!isset($params['id']) || empty($params['id'])){
            return array('status' => 0, 'info' => '未指定缓存管理ID');
        }
        $where = array('id' => $params['id']);
        $cache_info = $this->getCacheInfoByWhere($where);
        if(empty($cache_info) || empty($cache_info['cache_key'])){
            return array('status' => 0, 'info' => '缓存记录不存在');
        }
        $res = $this->cache_memcached->delete($cache_info['cache_key']);

        $data['cache_update_time'] = date('Y-m-d H:i:s', time());
        $this->models->admin_cache->updateById($params['id'], $data);
        if($res){
            $this->ret = array('status' => 1, 'info' => '更新缓存成功');
        }else{
            $this->ret = array('status' => 0, 'info' => '更新缓存失败');
        }
        return $this->ret;
    }

    public function deleteCacheById($cache_id = 0){
        if($cache_id){
            $where = array('id' => $cache_id);
            $res = $this->models->admin_cache->deleteByWhere($where);
            if($res){
                $this->ret = array('status' => 1, 'info' => '删除缓存管理成功');
            }else{
                $this->ret = array('status' => 0, 'info' => '删除缓存管理失败');
            }
        }else{
            $this->ret = array('status' => 0, 'info' => '未指定删除ID');
        }
        return $this->ret;
    }

    /**
     * 查询分组键名是否存在
     * @param string $cache_key
     * @param array $where
     * @return bool
     */
    private function checkCacheKeyExisted($cache_key = '', $where = array()){
        $existed = false;
        if($cache_key){
            $where['cache_key'] = trim($cache_key);
            $count = $this->models->admin_cache->getListCountByWhere($where);
            $count && $existed = true;
        }
        return $existed;
    }

    public function getCacheInfoByWhere($where = array()){
        return $this->models->admin_cache->getOneByWhere($where);
    }

    public function clearDomainSufixCacheByGroupId($group_id = 0){
        if($group_id){
            $where['id'] = $group_id;
            $group_info = $this->models->admin_setting_group->getOneByWhere($where);
            if(isset($group_info['groupkey'])
                && in_array($group_info['groupkey'], array(
                    self::TOP_DOMAIN_SUFFIX_GROUPKEY,
                    self::SENCOND_DOMAIN_SUFFIX_GROUPKEY
                ))){
                $res = $this->cache_memcached->delete(self::DOMAINSUFFIX_CACHE_KEY);
            }
        }

    }
}