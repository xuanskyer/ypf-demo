# ************************************************************
# Sequel Pro SQL dump
# Version 4529
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.48)
# Database: ypf_demo
# Generation Time: 2016-11-06 05:14:03 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table admin_cache
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_cache`;

CREATE TABLE `admin_cache` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `cache_key` varchar(255) CHARACTER SET latin1 NOT NULL COMMENT '缓存key',
  `cache_type` tinyint(1) DEFAULT '0' COMMENT '缓存类型:0-memcache',
  `description` varchar(255) DEFAULT NULL COMMENT '缓存描述',
  `status` tinyint(1) DEFAULT '0' COMMENT '缓存状态：',
  `created_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '添加时间',
  `cache_update_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '缓存最后更新时间',
  `updated_time` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_cache` (`cache_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='缓存key数据表';



# Dump of table admin_member_setting
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_member_setting`;

CREATE TABLE `admin_member_setting` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '自增',
  `member_id` int(10) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `setting_id` int(10) NOT NULL DEFAULT '0' COMMENT '配置ID',
  `group_id` int(10) NOT NULL DEFAULT '0' COMMENT '配置分组ID',
  `setting_value` varchar(100) NOT NULL COMMENT '配置项值',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '新增时间',
  `updated_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态：0-禁用，1-启用',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_setting` (`member_id`,`setting_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户-配置 关联表';



# Dump of table admin_operate_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_operate_log`;

CREATE TABLE `admin_operate_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '操作人ID',
  `nickname` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '用户昵称',
  `module` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '模块名',
  `controller` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '控制器',
  `action` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '方法',
  `params` text CHARACTER SET utf8 COMMENT '参数',
  `method` tinyint(1) unsigned DEFAULT '2' COMMENT '请求方式，2:get；8:post；0:未知',
  `addtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '操作时间',
  `ip` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '操作ip',
  `keyword` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '操作关键字',
  `message` varchar(500) CHARACTER SET utf8 DEFAULT NULL COMMENT '详细记录',
  `domain` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT '域名',
  `obj_user` int(10) unsigned DEFAULT NULL COMMENT '被操作的用户',
  `remark` varchar(500) CHARACTER SET utf8 DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `admin_operate_log` WRITE;
/*!40000 ALTER TABLE `admin_operate_log` DISABLE KEYS */;

INSERT INTO `admin_operate_log` (`id`, `user_id`, `nickname`, `module`, `controller`, `action`, `params`, `method`, `addtime`, `ip`, `keyword`, `message`, `domain`, `obj_user`, `remark`)
VALUES
	(1,1,'xuanskyer','Publicer','Login','check_login','{\"id\":\"1\",\"nickname\":\"xuanskyer\",\"keyword\":\"user_login\",\"method\":8,\"message\":\"\\u540e\\u53f0\\u767b\\u5f55\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 11:39:49','127.0.0.1','user_login','后台登录','',0,''),
	(2,1,'xuanskyer','Publicer','Login','check_login','{\"id\":\"1\",\"nickname\":\"xuanskyer\",\"keyword\":\"user_login\",\"method\":8,\"message\":\"\\u540e\\u53f0\\u767b\\u5f55\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 11:40:33','127.0.0.1','user_login','后台登录','',0,''),
	(3,1,'xuanskyer','Auth','Role','saveNode','{\"params\":\"{\\\"role_id\\\":\\\"1\\\",\\\"node_id\\\":\\\"26\\\",\\\"level\\\":\\\"2\\\"}\",\"message\":\"\\u4fdd\\u5b58\\u8282\\u70b9\\uff1a\\u4fdd\\u5b58\\u6210\\u529f\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 11:45:39','127.0.0.1','','保存节点：保存成功','',0,''),
	(4,1,'xuanskyer','Auth','UserLog','operate','{\"params\":\"{\\\"op\\\":\\\"list\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Auth\\\\\\\\UserLog\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 11:45:46','127.0.0.1','','权限错误：无权限','',0,''),
	(5,1,'xuanskyer','Auth','UserLog','operate','{\"params\":\"{\\\"op\\\":\\\"list\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Auth\\\\\\\\UserLog\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 11:45:54','127.0.0.1','','权限错误：无权限','',0,''),
	(6,1,'xuanskyer','Auth','UserLog','operate','{\"params\":\"{\\\"op\\\":\\\"list\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Auth\\\\\\\\UserLog\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 11:47:09','127.0.0.1','','权限错误：无权限','',0,''),
	(7,1,'xuanskyer','Auth','UserLog','operate','{\"params\":\"{\\\"op\\\":\\\"list\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Auth\\\\\\\\UserLog\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 11:47:54','127.0.0.1','','权限错误：无权限','',0,''),
	(8,1,'xuanskyer','Auth','UserLog','operate','{\"params\":\"{\\\"op\\\":\\\"list\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Auth\\\\\\\\UserLog\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 11:48:16','127.0.0.1','','权限错误：无权限','',0,''),
	(9,1,'xuanskyer','Auth','UserLog','operate','{\"params\":\"{\\\"op\\\":\\\"list\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Auth\\\\\\\\UserLog\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 11:51:34','127.0.0.1','','权限错误：无权限','',0,''),
	(10,1,'xuanskyer','Auth','UserLog','operate','{\"params\":\"{\\\"op\\\":\\\"list\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Auth\\\\\\\\UserLog\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 11:51:42','127.0.0.1','','权限错误：无权限','',0,''),
	(11,1,'xuanskyer','Auth','UserLog','operate','{\"params\":\"{\\\"op\\\":\\\"list\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Auth\\\\\\\\UserLog\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 11:53:13','127.0.0.1','','权限错误：无权限','',0,''),
	(12,1,'xuanskyer','Auth','UserLog','operate','{\"params\":\"{\\\"op\\\":\\\"list\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Auth\\\\\\\\UserLog\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 11:53:29','127.0.0.1','','权限错误：无权限','',0,''),
	(13,1,'xuanskyer','Auth','UserLog','operate','{\"params\":\"{\\\"op\\\":\\\"list\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Auth\\\\\\\\UserLog\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 11:53:50','127.0.0.1','','权限错误：无权限','',0,''),
	(14,1,'xuanskyer','Auth','UserLog','operate','{\"params\":\"{\\\"op\\\":\\\"list\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Auth\\\\\\\\UserLog\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 11:54:12','127.0.0.1','','权限错误：无权限','',0,''),
	(15,1,'xuanskyer','Auth','UserLog','operate','{\"params\":\"{\\\"op\\\":\\\"list\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Auth\\\\\\\\UserLog\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 11:54:45','127.0.0.1','','权限错误：无权限','',0,''),
	(16,1,'xuanskyer','Auth','UserLog','operate','{\"params\":\"{\\\"op\\\":\\\"list\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Auth\\\\\\\\UserLog\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 11:56:43','127.0.0.1','','权限错误：无权限','',0,''),
	(17,1,'xuanskyer','Auth','UserLog','operate','{\"params\":\"{\\\"op\\\":\\\"list\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Auth\\\\\\\\UserLog\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 11:58:53','127.0.0.1','','权限错误：无权限','',0,''),
	(18,1,'xuanskyer','Auth','UserLog','operate','{\"params\":\"{\\\"op\\\":\\\"list\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Auth\\\\\\\\UserLog\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 12:00:21','127.0.0.1','','权限错误：无权限','',0,''),
	(19,1,'xuanskyer','Auth','UserLog','operate','{\"params\":\"{\\\"op\\\":\\\"list\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Auth\\\\\\\\UserLog\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 12:01:13','127.0.0.1','','权限错误：无权限','',0,''),
	(20,1,'xuanskyer','Auth','UserLog','operate','{\"params\":\"{\\\"op\\\":\\\"list\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Auth\\\\\\\\UserLog\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 12:02:58','127.0.0.1','','权限错误：无权限','',0,''),
	(21,1,'xuanskyer','Auth','UserLog','operate','{\"params\":\"{\\\"op\\\":\\\"list\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Auth\\\\\\\\UserLog\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 12:04:37','127.0.0.1','','权限错误：无权限','',0,''),
	(22,1,'xuanskyer','Auth','UserLog','operate','{\"params\":\"{\\\"op\\\":\\\"list\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Auth\\\\\\\\UserLog\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 12:04:39','127.0.0.1','','权限错误：无权限','',0,''),
	(23,1,'xuanskyer','Auth','UserLog','operate','{\"params\":\"{\\\"op\\\":\\\"list\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Auth\\\\\\\\UserLog\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 12:05:51','127.0.0.1','','权限错误：无权限','',0,''),
	(24,1,'xuanskyer','Publicer','Login','logout','{\"message\":\"\\u9000\\u51fa\\u767b\\u5f55\",\"keyword\":\"user_logout\",\"method\":8,\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 12:06:21','127.0.0.1','user_logout','退出登录','',0,''),
	(25,1,'xuanskyer','Publicer','Login','check_login','{\"id\":\"1\",\"nickname\":\"xuanskyer\",\"keyword\":\"user_login\",\"method\":8,\"message\":\"\\u540e\\u53f0\\u767b\\u5f55\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 12:06:31','127.0.0.1','user_login','后台登录','',0,''),
	(26,1,'xuanskyer','Auth','UserLog','operate','{\"params\":\"{\\\"op\\\":\\\"list\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Auth\\\\\\\\UserLog\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 12:09:34','127.0.0.1','','权限错误：无权限','',0,''),
	(27,1,'xuanskyer','Auth','UserLog','operate','{\"params\":\"{\\\"op\\\":\\\"list\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Auth\\\\\\\\UserLog\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 12:09:36','127.0.0.1','','权限错误：无权限','',0,''),
	(28,1,'xuanskyer','Auth','UserLog','operate','{\"params\":\"{\\\"op\\\":\\\"list\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Auth\\\\\\\\UserLog\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 12:09:44','127.0.0.1','','权限错误：无权限','',0,''),
	(29,1,'xuanskyer','Auth','UserLog','operate','{\"params\":\"{\\\"op\\\":\\\"list\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Auth\\\\\\\\UserLog\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 12:09:48','127.0.0.1','','权限错误：无权限','',0,''),
	(30,1,'xuanskyer','Auth','Index','nodeAdd','{\"params\":\"{\\\"name\\\":\\\"list\\\",\\\"nav_name\\\":\\\"keng\\\",\\\"title\\\":\\\"list\\\",\\\"pid\\\":23,\\\"status\\\":1,\\\"level\\\":3}\",\"message\":\"\\u65b0\\u589e\\u8282\\u70b9\\uff1a\\u6dfb\\u52a0\\u65b0\\u65b9\\u6cd5\\u6210\\u529f\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 12:10:22','127.0.0.1','','新增节点：添加新方法成功','',0,''),
	(31,1,'xuanskyer','Auth','Role','saveNode','{\"params\":\"{\\\"role_id\\\":\\\"1\\\",\\\"node_id\\\":\\\"153\\\",\\\"level\\\":\\\"3\\\"}\",\"message\":\"\\u4fdd\\u5b58\\u8282\\u70b9\\uff1a\\u4fdd\\u5b58\\u6210\\u529f\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 12:10:34','127.0.0.1','','保存节点：保存成功','',0,''),
	(32,1,'xuanskyer','Auth','Index','operate','{\"message\":\"\\u7981\\u7528\\u8282\\u70b9\\uff1a\\u8be5\\u8282\\u70b9\\u5b58\\u5728\\u5b50\\u8282\\u70b9\\u65e0\\u6cd5\\u7981\\u7528\",\"params\":\"{\\\"id\\\":\\\"128\\\",\\\"status\\\":\\\"0\\\"}\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 12:24:37','127.0.0.1','','禁用节点：该节点存在子节点无法禁用','',0,''),
	(33,1,'xuanskyer','Auth','User','add','{\"params\":{\"user_name\":\"test\",\"nickname\":\"test\",\"role_id\":\"2\",\"source\":\"add\"},\"message\":\"\\u65b0\\u589e\\u8d26\\u6237\\uff1a\\u65b0\\u589e\\u7528\\u6237\\u6210\\u529f\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 12:24:52','127.0.0.1','','新增账户：新增用户成功','',0,''),
	(34,0,'xuanskyer','Publicer','Login','check_login','{\"id\":\"1\",\"nickname\":\"xuanskyer\",\"keyword\":\"user_login\",\"method\":8,\"message\":\"\\u540e\\u53f0\\u767b\\u5f55\\u5931\\u8d25\\uff1a\\u5bc6\\u7801\\u4e0d\\u6b63\\u786e\\uff01\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 12:31:53','127.0.0.1','user_login','后台登录失败：密码不正确！','',0,''),
	(35,1,'xuanskyer','Publicer','Login','check_login','{\"id\":\"1\",\"nickname\":\"xuanskyer\",\"keyword\":\"user_login\",\"method\":8,\"message\":\"\\u540e\\u53f0\\u767b\\u5f55\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 12:32:16','127.0.0.1','user_login','后台登录','',0,''),
	(36,1,'xuanskyer','Personal','Index','operate','{\"message\":\"\\u4fee\\u6539\\u5bc6\\u7801\\uff1a\\u65e7\\u5bc6\\u7801\\u9519\\u8bef\\uff01\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 12:32:42','127.0.0.1','','修改密码：旧密码错误！','',0,''),
	(37,1,'xuanskyer','Publicer','Login','logout','{\"message\":\"\\u9000\\u51fa\\u767b\\u5f55\",\"keyword\":\"user_logout\",\"method\":8,\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 12:37:34','127.0.0.1','user_logout','退出登录','',0,''),
	(38,1,'xuanskyer','Publicer','Login','check_login','{\"id\":\"1\",\"nickname\":\"xuanskyer\",\"keyword\":\"user_login\",\"method\":8,\"message\":\"\\u540e\\u53f0\\u767b\\u5f55\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 12:37:41','127.0.0.1','user_login','后台登录','',0,''),
	(39,1,'xuanskyer','Publicer','Login','logout','{\"message\":\"\\u9000\\u51fa\\u767b\\u5f55\",\"keyword\":\"user_logout\",\"method\":8,\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 12:39:00','127.0.0.1','user_logout','退出登录','',0,''),
	(40,1,'xuanskyer','Publicer','Login','check_login','{\"id\":\"1\",\"nickname\":\"xuanskyer\",\"keyword\":\"user_login\",\"method\":8,\"message\":\"\\u540e\\u53f0\\u767b\\u5f55\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 12:43:45','127.0.0.1','user_login','后台登录','',0,''),
	(41,1,'xuanskyer','Publicer','Login','check_login','{\"id\":\"1\",\"nickname\":\"xuanskyer\",\"keyword\":\"user_login\",\"method\":8,\"message\":\"\\u540e\\u53f0\\u767b\\u5f55\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 12:44:40','127.0.0.1','user_login','后台登录','',0,''),
	(42,1,'xuanskyer','Publicer','Login','logout','{\"message\":\"\\u9000\\u51fa\\u767b\\u5f55\",\"keyword\":\"user_logout\",\"method\":8,\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 13:02:41','127.0.0.1','user_logout','退出登录','',0,''),
	(43,1,'xuanskyer','Publicer','Login','check_login','{\"id\":\"1\",\"nickname\":\"xuanskyer\",\"keyword\":\"user_login\",\"method\":8,\"message\":\"\\u540e\\u53f0\\u767b\\u5f55\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 13:02:56','127.0.0.1','user_login','后台登录','',0,''),
	(44,1,'xuanskyer','Auth','Index','operate','{\"params\":\"[\\\"142\\\",\\\"143\\\",\\\"144\\\",\\\"145\\\",\\\"146\\\"]\",\"message\":\"\\u6279\\u91cf\\u5220\\u9664\\u65b9\\u6cd5\\u8282\\u70b9\\uff1a\\u6279\\u91cf\\u5220\\u9664\\u6210\\u529f\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 13:04:59','127.0.0.1','','批量删除方法节点：批量删除成功','',0,''),
	(45,1,'xuanskyer','Auth','Index','operate','{\"message\":\"\\u5220\\u9664\\u63a7\\u5236\\u5668\\u8282\\u70b9\\uff1a\\u5220\\u9664\\u6210\\u529f\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 13:05:04','127.0.0.1','','删除控制器节点：删除成功','',0,''),
	(46,1,'xuanskyer','Auth','Role','saveNode','{\"params\":\"null\",\"message\":\"\\u4fdd\\u5b58\\u8282\\u70b9\\uff1a\\u4fdd\\u5b58\\u6210\\u529f\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 13:05:20','127.0.0.1','','保存节点：保存成功','',0,''),
	(47,1,'xuanskyer','Develop','AutoDevModule','operate','{\"params\":\"{\\\"db_name\\\":\\\"demo\\\",\\\"table_name\\\":\\\"user\\\",\\\"op\\\":\\\"getTableFields\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Develop\\\\\\\\AutoDevModule\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 13:05:52','127.0.0.1','','权限错误：无权限','',0,''),
	(48,1,'xuanskyer','Develop','AutoDevModule','operate','{\"params\":\"{\\\"db_name\\\":\\\"demo\\\",\\\"table_name\\\":\\\"user\\\",\\\"op\\\":\\\"getTableFields\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Develop\\\\\\\\AutoDevModule\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 13:05:56','127.0.0.1','','权限错误：无权限','',0,''),
	(49,1,'xuanskyer','Develop','AutoDevModule','operate','{\"params\":\"{\\\"db_name\\\":\\\"demo\\\",\\\"table_name\\\":\\\"user\\\",\\\"op\\\":\\\"getTableFields\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Develop\\\\\\\\AutoDevModule\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 13:07:57','127.0.0.1','','权限错误：无权限','',0,''),
	(50,1,'xuanskyer','Develop','AutoDevModule','operate','{\"params\":\"{\\\"db_name\\\":\\\"demo\\\",\\\"table_name\\\":\\\"user\\\",\\\"op\\\":\\\"getTableFields\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Develop\\\\\\\\AutoDevModule\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 13:09:26','127.0.0.1','','权限错误：无权限','',0,''),
	(51,1,'xuanskyer','Publicer','Login','logout','{\"message\":\"\\u9000\\u51fa\\u767b\\u5f55\",\"keyword\":\"user_logout\",\"method\":8,\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 13:09:40','127.0.0.1','user_logout','退出登录','',0,''),
	(52,1,'xuanskyer','Publicer','Login','check_login','{\"id\":\"1\",\"nickname\":\"xuanskyer\",\"keyword\":\"user_login\",\"method\":8,\"message\":\"\\u540e\\u53f0\\u767b\\u5f55\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 13:09:48','127.0.0.1','user_login','后台登录','',0,''),
	(53,1,'xuanskyer','Develop','AutoDevModule','operate','{\"params\":\"{\\\"db_name\\\":\\\"demo\\\",\\\"table_name\\\":\\\"user\\\",\\\"op\\\":\\\"getTableFields\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Develop\\\\\\\\AutoDevModule\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 13:10:10','127.0.0.1','','权限错误：无权限','',0,''),
	(54,1,'xuanskyer','Develop','AutoDevModule','operate','{\"params\":\"{\\\"db_name\\\":\\\"demo\\\",\\\"table_name\\\":\\\"user\\\",\\\"op\\\":\\\"getTableFields\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Develop\\\\\\\\AutoDevModule\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 13:10:18','127.0.0.1','','权限错误：无权限','',0,''),
	(55,1,'xuanskyer','Develop','AutoDevModule','operate','{\"params\":\"{\\\"db_name\\\":\\\"demo\\\",\\\"table_name\\\":\\\"user\\\",\\\"op\\\":\\\"getTableFields\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Develop\\\\\\\\AutoDevModule\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 13:10:55','127.0.0.1','','权限错误：无权限','',0,''),
	(56,1,'xuanskyer','Develop','AutoDevModule','operate','{\"params\":\"{\\\"db_name\\\":\\\"demo\\\",\\\"table_name\\\":\\\"user\\\",\\\"op\\\":\\\"getTableFields\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Develop\\\\\\\\AutoDevModule\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 13:10:59','127.0.0.1','','权限错误：无权限','',0,''),
	(57,1,'xuanskyer','Develop','AutoDevModule','operate','{\"params\":\"{\\\"db_name\\\":\\\"demo\\\",\\\"table_name\\\":\\\"user\\\",\\\"op\\\":\\\"getTableFields\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Develop\\\\\\\\AutoDevModule\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 13:11:40','127.0.0.1','','权限错误：无权限','',0,''),
	(58,1,'xuanskyer','Develop','AutoDevModule','operate','{\"params\":\"{\\\"db_name\\\":\\\"demo\\\",\\\"table_name\\\":\\\"user\\\",\\\"op\\\":\\\"getTableFields\\\",\\\"route\\\":\\\"\\\\\\\\Controller\\\\\\\\Admin\\\\\\\\Develop\\\\\\\\AutoDevModule\\\\\\\\operate\\\"}\",\"message\":\"\\u6743\\u9650\\u9519\\u8bef\\uff1a\\u65e0\\u6743\\u9650\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 13:12:03','127.0.0.1','','权限错误：无权限','',0,''),
	(59,1,'xuanskyer','Auth','Index','nodeAdd','{\"params\":\"{\\\"name\\\":\\\"getTableFields\\\",\\\"title\\\":\\\"\\\\u83b7\\\\u53d6\\\\u6570\\\\u636e\\\\u8868\\\\u5b57\\\\u6bb5\\\",\\\"pid\\\":126,\\\"status\\\":1,\\\"level\\\":3}\",\"message\":\"\\u65b0\\u589e\\u8282\\u70b9\\uff1a\\u6dfb\\u52a0\\u65b0\\u65b9\\u6cd5\\u6210\\u529f\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 13:12:40','127.0.0.1','','新增节点：添加新方法成功','',0,''),
	(60,1,'xuanskyer','Auth','Role','saveNode','{\"params\":\"{\\\"role_id\\\":\\\"1\\\",\\\"node_id\\\":\\\"154\\\",\\\"level\\\":\\\"3\\\"}\",\"message\":\"\\u4fdd\\u5b58\\u8282\\u70b9\\uff1a\\u4fdd\\u5b58\\u6210\\u529f\",\"keyword\":\"\",\"obj_user\":\"\",\"domain\":\"\",\"remark\":\"\"}',8,'2016-11-06 13:12:55','127.0.0.1','','保存节点：保存成功','',0,'');

/*!40000 ALTER TABLE `admin_operate_log` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table admin_setting
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_setting`;

CREATE TABLE `admin_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `config_title` varchar(255) NOT NULL,
  `config_key` varchar(255) NOT NULL,
  `config_value` varchar(255) NOT NULL,
  `config_description` varchar(255) NOT NULL,
  `config_groupid` int(11) NOT NULL,
  `config_sort` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  `updated_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unq_config_key` (`config_key`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table admin_setting_group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_setting_group`;

CREATE TABLE `admin_setting_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_time` datetime NOT NULL,
  `updated_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `groupkey` varchar(100) DEFAULT NULL COMMENT '唯一键值',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_key` (`groupkey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table admin_version_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_version_log`;

CREATE TABLE `admin_version_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL COMMENT '版本更新标题',
  `content` varchar(1024) DEFAULT NULL COMMENT '版本更新LOG内容详情',
  `create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '新增时间',
  `update_time` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `type` varchar(20) DEFAULT NULL COMMENT '日志类型',
  `status` tinyint(1) DEFAULT '1' COMMENT 'log状态',
  `user_id` int(10) DEFAULT '0' COMMENT '操作人ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='admin-v3系统更新日志';



# Dump of table member
# ------------------------------------------------------------

DROP TABLE IF EXISTS `member`;

CREATE TABLE `member` (
  `id` mediumint(6) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(64) NOT NULL,
  `dept_id` smallint(3) NOT NULL DEFAULT '1',
  `username` varchar(64) NOT NULL DEFAULT '',
  `password` char(32) NOT NULL DEFAULT '',
  `email` varchar(64) NOT NULL DEFAULT '',
  `account_type` tinyint(1) unsigned DEFAULT '1' COMMENT '用户类型:  1-个人，2-政企，3-代理',
  `pid` int(10) unsigned DEFAULT '0',
  `mobile` char(11) NOT NULL DEFAULT '',
  `qq` varchar(20) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `remark` varchar(256) NOT NULL DEFAULT '',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `login_time` int(11) NOT NULL DEFAULT '0',
  `check` mediumint(8) NOT NULL DEFAULT '0',
  `micro` varchar(64) NOT NULL,
  `source` varchar(64) NOT NULL DEFAULT '',
  `property` int(3) unsigned NOT NULL DEFAULT '2' COMMENT 'ä¼ä¸š1,ä¸ªäºº2,ä»£ç†3',
  `api_uid` varchar(100) NOT NULL DEFAULT '0',
  `balance` decimal(8,2) NOT NULL DEFAULT '0.00',
  `type` int(3) NOT NULL DEFAULT '0',
  `pay_type` int(3) NOT NULL DEFAULT '4',
  `account_check` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 -审核不通过 1 -审核通过',
  `dns_server_used` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否为dns服务器指定账户',
  `account_check_reason` varchar(80) DEFAULT NULL COMMENT '政企审核不通过原因',
  `agent_plat_type` int(10) NOT NULL DEFAULT '0' COMMENT '所属代理平台类型：0-官网，1-红网卫士，2-中测云盾',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`) USING BTREE,
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table rbac_access
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rbac_access`;

CREATE TABLE `rbac_access` (
  `role_id` smallint(6) unsigned NOT NULL,
  `node_id` smallint(6) unsigned NOT NULL,
  `level` tinyint(1) NOT NULL,
  `module` varchar(50) DEFAULT NULL,
  `int` int(10) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`int`),
  KEY `groupId` (`role_id`),
  KEY `nodeId` (`node_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `rbac_access` WRITE;
/*!40000 ALTER TABLE `rbac_access` DISABLE KEYS */;

INSERT INTO `rbac_access` (`role_id`, `node_id`, `level`, `module`, `int`)
VALUES
	(1,1,1,NULL,1),
	(1,2,1,NULL,2),
	(1,3,2,NULL,3),
	(1,4,2,NULL,4),
	(1,5,2,NULL,5),
	(1,6,2,NULL,6),
	(1,7,3,NULL,7),
	(1,8,3,NULL,8),
	(1,9,3,NULL,9),
	(1,10,3,NULL,10),
	(1,11,3,NULL,11),
	(1,12,3,NULL,12),
	(1,13,3,NULL,13),
	(1,14,3,NULL,14),
	(1,15,3,NULL,15),
	(1,16,3,NULL,16),
	(1,17,3,NULL,17),
	(1,18,3,NULL,18),
	(1,19,3,NULL,19),
	(1,20,3,NULL,20),
	(1,21,3,NULL,21),
	(1,27,2,NULL,22),
	(1,28,3,NULL,23),
	(1,29,3,NULL,24),
	(1,105,3,NULL,25),
	(1,108,3,NULL,26),
	(1,109,3,NULL,27),
	(1,110,3,NULL,28),
	(1,111,3,NULL,29),
	(1,112,3,NULL,30),
	(1,113,3,NULL,31),
	(1,42,3,NULL,32),
	(1,43,3,NULL,33),
	(1,44,3,NULL,34),
	(1,45,3,NULL,35),
	(1,46,3,NULL,36),
	(1,47,3,NULL,37),
	(1,48,3,NULL,38),
	(1,49,3,NULL,39),
	(1,97,3,NULL,40),
	(1,98,3,NULL,41),
	(1,99,3,NULL,42),
	(1,100,3,NULL,43),
	(1,101,3,NULL,44),
	(1,102,3,NULL,45),
	(1,103,3,NULL,46),
	(1,104,3,NULL,47),
	(1,41,3,NULL,48),
	(1,22,2,NULL,49),
	(1,81,3,NULL,50),
	(1,82,3,NULL,51),
	(1,83,3,NULL,52),
	(1,84,3,NULL,53),
	(1,85,3,NULL,54),
	(1,86,3,NULL,55),
	(1,87,3,NULL,56),
	(1,88,3,NULL,57),
	(1,89,3,NULL,58),
	(1,90,3,NULL,59),
	(1,91,3,NULL,60),
	(1,92,3,NULL,61),
	(1,93,3,NULL,62),
	(1,23,2,NULL,63),
	(1,94,3,NULL,64),
	(1,95,3,NULL,65),
	(1,96,3,NULL,66),
	(1,24,2,NULL,67),
	(1,67,3,NULL,68),
	(1,68,3,NULL,69),
	(1,69,3,NULL,70),
	(1,70,3,NULL,71),
	(1,71,3,NULL,72),
	(1,72,3,NULL,73),
	(1,73,3,NULL,74),
	(1,74,3,NULL,75),
	(1,75,3,NULL,76),
	(1,76,3,NULL,77),
	(1,77,3,NULL,78),
	(1,78,3,NULL,79),
	(1,79,3,NULL,80),
	(1,80,3,NULL,81),
	(1,25,2,NULL,82),
	(1,58,3,NULL,83),
	(1,59,3,NULL,84),
	(1,60,3,NULL,85),
	(1,61,3,NULL,86),
	(1,62,3,NULL,87),
	(1,63,3,NULL,88),
	(1,64,3,NULL,89),
	(1,65,3,NULL,90),
	(1,66,3,NULL,91),
	(1,114,1,NULL,112),
	(1,50,3,NULL,93),
	(1,51,3,NULL,94),
	(1,52,3,NULL,95),
	(1,53,3,NULL,96),
	(1,54,3,NULL,97),
	(1,55,3,NULL,98),
	(1,56,3,NULL,99),
	(1,57,3,NULL,100),
	(1,30,3,NULL,101),
	(1,31,3,NULL,102),
	(1,32,3,NULL,103),
	(1,33,3,NULL,104),
	(1,34,3,NULL,105),
	(1,35,3,NULL,106),
	(1,36,3,NULL,107),
	(1,37,3,NULL,108),
	(1,38,3,NULL,109),
	(1,39,3,NULL,110),
	(1,40,3,NULL,111),
	(1,115,2,NULL,113),
	(1,116,3,NULL,114),
	(1,117,3,NULL,115),
	(1,118,3,NULL,116),
	(1,119,3,NULL,117),
	(1,120,3,NULL,118),
	(1,121,3,NULL,119),
	(1,122,3,NULL,120),
	(1,123,1,NULL,121),
	(1,124,2,NULL,122),
	(1,125,2,NULL,123),
	(1,126,2,NULL,124),
	(1,127,2,NULL,125),
	(1,128,2,NULL,126),
	(1,129,3,NULL,127),
	(1,130,3,NULL,128),
	(1,131,3,NULL,129),
	(1,132,3,NULL,130),
	(1,133,3,NULL,131),
	(1,134,3,NULL,132),
	(1,135,3,NULL,133),
	(1,136,3,NULL,134),
	(1,137,3,NULL,135),
	(1,138,3,NULL,136),
	(1,139,3,NULL,137),
	(1,140,3,NULL,138),
	(1,141,3,NULL,139),
	(1,142,3,NULL,140),
	(1,143,3,NULL,141),
	(1,144,3,NULL,142),
	(1,145,3,NULL,143),
	(1,146,3,NULL,144),
	(1,147,3,NULL,145),
	(1,148,3,NULL,146),
	(1,149,3,NULL,147),
	(1,150,3,NULL,148),
	(1,151,3,NULL,149),
	(1,152,3,NULL,150),
	(1,26,2,NULL,151),
	(1,153,3,NULL,152),
	(1,154,3,NULL,153);

/*!40000 ALTER TABLE `rbac_access` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rbac_node
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rbac_node`;

CREATE TABLE `rbac_node` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `nav_name` varchar(100) NOT NULL COMMENT '对应菜单栏名称',
  `title` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `remark` varchar(255) DEFAULT NULL,
  `sort` smallint(6) unsigned DEFAULT '9999',
  `pid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `display` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否显示：0-不显示，1-显示（默认）',
  PRIMARY KEY (`id`),
  KEY `level` (`level`),
  KEY `pid` (`pid`),
  KEY `status` (`status`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `rbac_node` WRITE;
/*!40000 ALTER TABLE `rbac_node` DISABLE KEYS */;

INSERT INTO `rbac_node` (`id`, `name`, `nav_name`, `title`, `status`, `remark`, `sort`, `pid`, `level`, `display`)
VALUES
	(1,'auth','','权限管理',1,NULL,1,0,1,1),
	(2,'Index','','后台首页',1,NULL,0,0,1,1),
	(3,'Index','','控制板',1,NULL,9999,2,2,1),
	(4,'role','','角色授权',1,NULL,9999,1,2,1),
	(5,'user','','用户中心',1,NULL,9999,1,2,1),
	(6,'nav','','顶部导航管理',1,NULL,9999,1,2,1),
	(7,'index','','index',1,NULL,9999,4,3,1),
	(8,'index','','index',1,NULL,9999,6,3,1),
	(9,'getNavList','','getNavList',1,NULL,9999,6,3,1),
	(10,'edit','','edit',1,NULL,9999,6,3,1),
	(11,'navSave','','navSave',1,NULL,9999,6,3,1),
	(12,'navAdd','','navAdd',1,NULL,9999,6,3,1),
	(13,'navEdit','','navEdit',1,NULL,9999,6,3,1),
	(14,'navDelete','','navDelete',1,NULL,9999,6,3,1),
	(15,'setNavStatus','','setNavStatus',1,NULL,9999,6,3,1),
	(16,'getNavNodeList','','getNavNodeList',1,NULL,9999,6,3,1),
	(17,'getNavBasic','','getNavBasic',1,NULL,9999,6,3,1),
	(18,'getNavNodeList','','getNavNodeList',1,NULL,9999,6,3,1),
	(19,'controlAdd','','controlAdd',1,NULL,9999,6,3,1),
	(20,'controlSave','','controlSave',1,NULL,9999,6,3,1),
	(21,'controlDelete','','controlDelete',1,NULL,9999,6,3,1),
	(114,'Personal','','个人中心',1,NULL,9999,0,1,1),
	(23,'UserLog','','UserLog',1,NULL,9999,1,2,1),
	(26,'User','','用户管理',1,NULL,9999,1,2,1),
	(27,'Index','','节点管理',1,NULL,9999,1,2,1),
	(28,'index','','节点列表',1,NULL,9999,27,3,1),
	(29,'nodeSave','','节点保存',1,NULL,9999,27,3,1),
	(30,'getNodeList','','getNodeList',1,NULL,9999,27,3,1),
	(31,'edit','','edit',1,NULL,9999,27,3,1),
	(32,'getClassNode','','getClassNode',1,NULL,9999,27,3,1),
	(33,'nodeEdit','','nodeEdit',1,NULL,9999,27,3,1),
	(34,'nodeDelete','','nodeDelete',1,NULL,9999,27,3,1),
	(35,'controlDelete','','controlDelete',1,NULL,9999,27,3,1),
	(36,'nodeAdd','','nodeAdd',1,NULL,9999,27,3,1),
	(37,'nodeBatchAdd','','nodeBatchAdd',1,NULL,9999,27,3,1),
	(38,'controlAdd','','controlAdd',1,NULL,9999,27,3,1),
	(39,'controlSave','','controlSave',1,NULL,9999,27,3,1),
	(40,'setControlNodeStatus','','setControlNodeStatus',1,NULL,9999,27,3,1),
	(41,'controlBatchAdd','','controlBatchAdd',1,NULL,9999,6,3,1),
	(42,'getRoleList','','getRoleList',1,NULL,9999,4,3,1),
	(43,'editStatus','','editStatus',1,NULL,9999,4,3,1),
	(44,'Manager','','Manager',1,NULL,9999,4,3,1),
	(45,'getNode','','getNode',1,NULL,9999,4,3,1),
	(46,'saveNode','','saveNode',1,NULL,9999,4,3,1),
	(47,'add','','add',1,NULL,9999,4,3,1),
	(48,'delete','','delete',1,NULL,9999,4,3,1),
	(49,'saveRole','','saveRole',1,NULL,9999,4,3,1),
	(50,'index','','index',1,NULL,9999,26,3,1),
	(51,'getUserList','','getUserList',1,NULL,9999,26,3,1),
	(52,'editStatus','','editStatus',1,NULL,9999,26,3,1),
	(53,'setRole','','setRole',1,NULL,9999,26,3,1),
	(54,'add','','add',1,NULL,9999,26,3,1),
	(55,'resetPasswd','','resetPasswd',1,NULL,9999,26,3,1),
	(56,'editUser','','editUser',1,NULL,9999,26,3,1),
	(57,'deleteUser','','deleteUser',1,NULL,9999,26,3,1),
	(58,'index','','index',1,NULL,9999,25,3,1),
	(59,'getRoleList','','getRoleList',1,NULL,9999,25,3,1),
	(60,'editStatus','','editStatus',1,NULL,9999,25,3,1),
	(61,'Manager','','Manager',1,NULL,9999,25,3,1),
	(62,'getNode','','getNode',1,NULL,9999,25,3,1),
	(63,'saveNode','','saveNode',1,NULL,9999,25,3,1),
	(64,'add','','add',1,NULL,9999,25,3,1),
	(65,'delete','','delete',1,NULL,9999,25,3,1),
	(66,'saveRole','','saveRole',1,NULL,9999,25,3,1),
	(67,'index','','index',1,NULL,9999,24,3,1),
	(68,'getNavList','','getNavList',1,NULL,9999,24,3,1),
	(69,'getNavBasic','','getNavBasic',1,NULL,9999,24,3,1),
	(70,'edit','','edit',1,NULL,9999,24,3,1),
	(71,'navSave','','navSave',1,NULL,9999,24,3,1),
	(72,'navAdd','','navAdd',1,NULL,9999,24,3,1),
	(73,'navEdit','','navEdit',1,NULL,9999,24,3,1),
	(74,'navDelete','','navDelete',1,NULL,9999,24,3,1),
	(75,'setNavStatus','','setNavStatus',1,NULL,9999,24,3,1),
	(76,'getNavNodeList','','getNavNodeList',1,NULL,9999,24,3,1),
	(77,'controlAdd','','controlAdd',1,NULL,9999,24,3,1),
	(78,'controlBatchAdd','','controlBatchAdd',1,NULL,9999,24,3,1),
	(79,'controlSave','','controlSave',1,NULL,9999,24,3,1),
	(80,'controlDelete','','controlDelete',1,NULL,9999,24,3,1),
	(81,'index','','index',1,NULL,9999,22,3,1),
	(82,'getNodeList','','getNodeList',1,NULL,9999,22,3,1),
	(83,'edit','','edit',1,NULL,9999,22,3,1),
	(84,'getClassNode','','getClassNode',1,NULL,9999,22,3,1),
	(85,'nodeSave','','nodeSave',1,NULL,9999,22,3,1),
	(86,'nodeEdit','','nodeEdit',1,NULL,9999,22,3,1),
	(87,'nodeDelete','','nodeDelete',1,NULL,9999,22,3,1),
	(88,'controlDelete','','controlDelete',1,NULL,9999,22,3,1),
	(89,'nodeAdd','','nodeAdd',1,NULL,9999,22,3,1),
	(90,'nodeBatchAdd','','nodeBatchAdd',1,NULL,9999,22,3,1),
	(91,'controlAdd','','controlAdd',1,NULL,9999,22,3,1),
	(92,'controlSave','','controlSave',1,NULL,9999,22,3,1),
	(93,'setControlNodeStatus','','setControlNodeStatus',1,NULL,9999,22,3,1),
	(94,'index','','index',1,NULL,9999,23,3,1),
	(95,'operate','','operate',1,NULL,9999,23,3,1),
	(96,'exportAuthLog','','exportAuthLog',1,NULL,9999,23,3,1),
	(97,'index','','index',1,NULL,9999,5,3,1),
	(98,'getUserList','','getUserList',1,NULL,9999,5,3,1),
	(99,'editStatus','','editStatus',1,NULL,9999,5,3,1),
	(100,'setRole','','setRole',1,NULL,9999,5,3,1),
	(101,'add','','add',1,NULL,9999,5,3,1),
	(102,'resetPasswd','','resetPasswd',1,NULL,9999,5,3,1),
	(103,'editUser','','editUser',1,NULL,9999,5,3,1),
	(104,'deleteUser','','deleteUser',1,NULL,9999,5,3,1),
	(105,'getRoleList','','角色列表',1,NULL,9999,4,3,1),
	(108,'Manager','','管理',1,NULL,9999,4,3,1),
	(109,'getNode','','getNode',1,'',9999,4,3,1),
	(110,'saveNode','','saveNode',1,'',9999,4,3,1),
	(111,'add','','add',1,'',9999,4,3,1),
	(112,'delete','','delete',1,'',9999,4,3,1),
	(113,'saveRole','','saveRole',1,'',9999,4,3,1),
	(115,'Index','','默认',1,NULL,9999,114,2,1),
	(116,'index','','index',1,NULL,9999,115,3,1),
	(117,'saveBasic','','saveBasic',1,NULL,9999,115,3,1),
	(118,'editPass','','editPass',1,NULL,9999,115,3,1),
	(119,'editStatus','','editStatus',1,NULL,9999,115,3,1),
	(120,'getLoggedMemberInfo','','getLoggedMemberInfo',1,NULL,9999,115,3,1),
	(121,'Manager','','Manager',1,NULL,9999,115,3,1),
	(122,'getUserNode','','getUserNode',1,NULL,9999,115,3,1),
	(123,'Develop','','开发管理',1,NULL,9999,0,1,1),
	(126,'AutoDevModule','','自动化模块开发',1,NULL,9999,123,2,1),
	(154,'getTableFields','','获取数据表字段',1,NULL,9999,126,3,1),
	(128,'VersionLog','','VersionLog',1,NULL,9999,123,2,1),
	(133,'index','','入口方法',1,NULL,9999,126,3,1),
	(134,'getDataList','','查询数据列表',1,NULL,9999,126,3,1),
	(135,'autoCreateModule','','自动创建模块',1,NULL,9999,126,3,1),
	(136,'addData','','新增',1,NULL,9999,126,3,1),
	(137,'batchAddData','','批量新增',1,NULL,9999,126,3,1),
	(138,'batchDeleteData','','批量删除',1,NULL,9999,126,3,1),
	(139,'editData','','编辑',1,NULL,9999,126,3,1),
	(140,'deleteData','','删除',1,NULL,9999,126,3,1),
	(141,'changeStatus','','修改审核状态',1,NULL,9999,126,3,1),
	(147,'index','','index',1,NULL,9999,128,3,1),
	(148,'getVersionLogList','','getVersionLogList',1,NULL,9999,128,3,1),
	(149,'add','','add',1,NULL,9999,128,3,1),
	(150,'edit','','edit',1,NULL,9999,128,3,1),
	(151,'delete','','delete',1,NULL,9999,128,3,1),
	(152,'batchDelete','','batchDelete',1,NULL,9999,128,3,1),
	(153,'list','keng','list',1,NULL,9999,23,3,1);

/*!40000 ALTER TABLE `rbac_node` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rbac_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rbac_role`;

CREATE TABLE `rbac_role` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `pid` smallint(6) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `rbac_role` WRITE;
/*!40000 ALTER TABLE `rbac_role` DISABLE KEYS */;

INSERT INTO `rbac_role` (`id`, `name`, `pid`, `status`, `remark`)
VALUES
	(1,'超级管理员',0,1,NULL),
	(2,'管理员',0,1,NULL);

/*!40000 ALTER TABLE `rbac_role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rbac_role_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rbac_role_user`;

CREATE TABLE `rbac_role_user` (
  `role_id` mediumint(9) unsigned DEFAULT NULL,
  `user_id` char(32) DEFAULT NULL,
  `int` int(10) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`int`),
  KEY `group_id` (`role_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `rbac_role_user` WRITE;
/*!40000 ALTER TABLE `rbac_role_user` DISABLE KEYS */;

INSERT INTO `rbac_role_user` (`role_id`, `user_id`, `int`)
VALUES
	(1,'1',1),
	(2,'65',2);

/*!40000 ALTER TABLE `rbac_role_user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `account` varchar(64) NOT NULL,
  `nickname` varchar(50) NOT NULL,
  `password` char(32) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `source` varchar(64) NOT NULL DEFAULT 'YunDun',
  `mobile` varchar(20) NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '最后一次登录时间',
  `avatar` varchar(200) NOT NULL,
  `error_passwd_count` int(10) NOT NULL DEFAULT '0' COMMENT '密码错误次数',
  `error_passwd_last` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '上次错误时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `account` (`account`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `account`, `nickname`, `password`, `remark`, `status`, `source`, `mobile`, `last_login`, `avatar`, `error_passwd_count`, `error_passwd_last`)
VALUES
	(1,'xuanskyer','xuanskyer','7ea9eb7b6cebd41db07aa948b438edd1','世界不可能那么远啊！',1,'','','2016-11-06 13:09:48','',0,'2016-11-06 12:31:53'),
	(65,'test','test','7ea9eb7b6cebd41db07aa948b438edd1','',1,'add','','0000-00-00 00:00:00','',0,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_cache_keys
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_cache_keys`;

CREATE TABLE `user_cache_keys` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) NOT NULL COMMENT '缓存分组名',
  `cache_key` varchar(250) NOT NULL COMMENT 'memcache缓存KEY',
  `type` varchar(50) NOT NULL DEFAULT 'memcache' COMMENT '缓存类型',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '新增时间',
  `update_time` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_key` (`cache_key`,`type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户RBAC权限缓存KEY';

LOCK TABLES `user_cache_keys` WRITE;
/*!40000 ALTER TABLE `user_cache_keys` DISABLE KEYS */;

INSERT INTO `user_cache_keys` (`id`, `group_name`, `cache_key`, `type`, `create_time`, `update_time`)
VALUES
	(1,'1','adminv3rbac#1#1#GetUserLevel1List','memcache','2016-11-06 11:45:02','0000-00-00 00:00:00'),
	(2,'all','adminv3rbac#all#3#GetNodeListByIds','memcache','2016-11-06 11:45:02','0000-00-00 00:00:00'),
	(3,'1','adminv3rbac#1#1#2#GetUserLevel2List','memcache','2016-11-06 11:45:02','0000-00-00 00:00:00'),
	(4,'all','adminv3rbac#all#4#5#6#27#23#GetNodeListByIds','memcache','2016-11-06 11:45:02','0000-00-00 00:00:00'),
	(5,'1','adminv3rbac#1#1#1#GetUserLevel2List','memcache','2016-11-06 11:45:02','0000-00-00 00:00:00'),
	(6,'all','adminv3rbac#all#115#GetNodeListByIds','memcache','2016-11-06 11:45:02','0000-00-00 00:00:00'),
	(7,'1','adminv3rbac#1#1#114#GetUserLevel2List','memcache','2016-11-06 11:45:02','0000-00-00 00:00:00'),
	(8,'all','adminv3rbac#all#126#127#128#GetNodeListByIds','memcache','2016-11-06 11:45:02','0000-00-00 00:00:00'),
	(9,'1','adminv3rbac#1#1#123#GetUserLevel2List','memcache','2016-11-06 11:45:02','0000-00-00 00:00:00'),
	(10,'all','adminv3rbac#all#index#GetNodeIdByName','memcache','2016-11-06 11:45:02','0000-00-00 00:00:00'),
	(11,'1','adminv3rbac#1#1#GetAccessList','memcache','2016-11-06 11:45:05','0000-00-00 00:00:00'),
	(12,'all','adminv3rbac#all#auth#GetNodeIdByName','memcache','2016-11-06 11:45:05','0000-00-00 00:00:00'),
	(13,'all','adminv3rbac#all#4#5#6#27#23#26#GetNodeListByIds','memcache','2016-11-06 11:45:42','0000-00-00 00:00:00'),
	(14,'1','rbac#1#1#GetAccessList','memcache','2016-11-06 12:05:51','0000-00-00 00:00:00'),
	(15,'1','rbac#1#1#GetUserLevel1List','memcache','2016-11-06 12:06:08','0000-00-00 00:00:00'),
	(16,'all','rbac#all#3#GetNodeListByIds','memcache','2016-11-06 12:06:08','0000-00-00 00:00:00'),
	(17,'1','rbac#1#1#2#GetUserLevel2List','memcache','2016-11-06 12:06:08','0000-00-00 00:00:00'),
	(18,'all','rbac#all#4#5#6#27#23#26#GetNodeListByIds','memcache','2016-11-06 12:06:08','0000-00-00 00:00:00'),
	(19,'1','rbac#1#1#1#GetUserLevel2List','memcache','2016-11-06 12:06:08','0000-00-00 00:00:00'),
	(20,'all','rbac#all#115#GetNodeListByIds','memcache','2016-11-06 12:06:08','0000-00-00 00:00:00'),
	(21,'1','rbac#1#1#114#GetUserLevel2List','memcache','2016-11-06 12:06:08','0000-00-00 00:00:00'),
	(22,'all','rbac#all#126#127#128#GetNodeListByIds','memcache','2016-11-06 12:06:08','0000-00-00 00:00:00'),
	(23,'1','rbac#1#1#123#GetUserLevel2List','memcache','2016-11-06 12:06:08','0000-00-00 00:00:00'),
	(24,'all','rbac#all#personal#GetNodeIdByName','memcache','2016-11-06 12:06:08','0000-00-00 00:00:00'),
	(25,'all','rbac#all#auth#GetNodeIdByName','memcache','2016-11-06 12:06:10','0000-00-00 00:00:00'),
	(26,'all','rbac#all#develop#GetNodeIdByName','memcache','2016-11-06 12:10:53','0000-00-00 00:00:00'),
	(27,'all','rbac#all#index#GetNodeIdByName','memcache','2016-11-06 12:14:30','0000-00-00 00:00:00'),
	(28,'all','rbac#all#126#128#GetNodeListByIds','memcache','2016-11-06 13:05:04','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `user_cache_keys` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_log`;

CREATE TABLE `user_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `table` varchar(255) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `ip` varchar(255) NOT NULL,
  `message` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_log` WRITE;
/*!40000 ALTER TABLE `user_log` DISABLE KEYS */;

INSERT INTO `user_log` (`id`, `user_id`, `type`, `table`, `datetime`, `ip`, `message`)
VALUES
	(1,1,'login','user','2016-11-06 11:39:49','127.0.0.1','<a>登陆</a>后台'),
	(2,1,'login','user','2016-11-06 11:40:33','127.0.0.1','<a>登陆</a>后台'),
	(3,1,'login','user','2016-11-06 12:06:31','127.0.0.1','<a>登陆</a>后台'),
	(4,1,'login','user','2016-11-06 12:32:16','127.0.0.1','<a>登陆</a>后台'),
	(5,1,'login','user','2016-11-06 12:37:41','127.0.0.1','<a>登陆</a>后台'),
	(6,1,'login','user','2016-11-06 12:43:45','127.0.0.1','<a>登陆</a>后台'),
	(7,1,'login','user','2016-11-06 12:44:40','127.0.0.1','<a>登陆</a>后台'),
	(8,1,'login','user','2016-11-06 13:02:56','127.0.0.1','<a>登陆</a>后台'),
	(9,1,'login','user','2016-11-06 13:09:48','127.0.0.1','<a>登陆</a>后台');

/*!40000 ALTER TABLE `user_log` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
