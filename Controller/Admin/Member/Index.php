<?php
namespace Controller\Admin\Member;
class Index extends \Controller\Admin\Common\Common{
	
	public function index(){
        $layout_params = array(
            'header' => '',
            'footer' => '',
            'slide'  => array('member')
        );
        $this->setHeaderFooter($layout_params);
        $member = new \Model\Member();

//        $where['id'] = 84;
//        $where['nickname'] = 'nickname';
        $where['dept_id'] = 1;
        $where['status'] = 1;

        $order['id'] = 'asc';
        $order['pay_type'] = 'asc';

        $fields = array('id, nickname, status');

        $offset = 0;
        $limit = 10;
        $list = $member->getListByWhere($where, $offset, $limit, $order, $fields);
//        var_dump($list);
        $this->view->display('Admin/Member/index.html');

	}
}
