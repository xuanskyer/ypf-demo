<?php
namespace Controller\Admin\Setting;
class Index extends \Controller\Admin\Common\Common{

    public $settingService;
    public $statusService;
    public function __construct(){
        parent::__construct();
        $this->setHeaderFooter();
        $this->settingService = new \Service\Setting\SettingService();
    }

    public function index(){

        $actions['getGroupList'] = \Url::get_function_url('setting', 'index', 'getGroupList',array(),true);
        $actions['getSettingList'] = \Url::get_function_url('setting', 'index', 'getSettingList',array(),true);
        $actions['addSetting'] = \Url::get_function_url('setting', 'index', 'addSetting',array(),true);
        $actions['addGroup'] = \Url::get_function_url('setting', 'index', 'addGroup',array(),true);
        $actions['editGroup'] = \Url::get_function_url('setting', 'index', 'editGroup',array(),true);
        $actions['deleteGroup'] = \Url::get_function_url('setting', 'index', 'deleteGroup',array(),true);
        $actions['changeStatus'] = \Url::get_function_url('setting', 'index', 'changeStatus',array(),true);
        $actions['managerSetting'] = \Url::get_function_url('setting', 'index', 'managerSetting',array(),true);
        $this->view->assign('actions' ,$actions);
        $this->view->display('Admin/Setting/index.html');
    }

    public function getGroupList(){

        $params = $this->parseJplistStatuses($this->req['statuses']);
        $this->ret = $this->settingService->getGroupList($params);
        $this->ajaxReturn($this->ret);
    }

    public function getSettingList(){

        $params = $this->parseJplistStatuses($this->req['statuses']);
        $params['where']['config_groupid'] = $this->req['group_id'];
        $this->ret = $this->settingService->getSettingList($params);
        $result['list'] = $this->ret['data'];
        if($this->ret['count']){
            $result['group_list'] = $this->settingService->getAllGroup();
        }
        $this->ret['data'] = $result;
        $this->ajaxReturn($this->ret);
    }

    /**
     * 修改配置分组状态
     */
    public function changeStatus(){
        $statusModel = new \Model\AdminSettingGroup();
        $this->changeStatusCommon($statusModel);
        $log_params['params'] = json_encode($this->post);
        $log_params['message'] = "修改配置分组状态：{$this->ret['info']}";
        $this->saveLog($log_params);
        $this->ajaxReturn($this->ret);
    }

    public function addGroup(){
        if(IS_POST){
            $this->ret = $this->settingService->addGroup($this->post);
            $log_params['params'] = json_encode($this->post);
            $log_params['message'] = "新增分组：{$this->ret['info']}";
            $this->saveLog($log_params);
        }elseif(IS_GET){

        }
        $this->ajaxReturn($this->ret);
    }

    public function editGroup(){
        if(IS_POST){
            $this->ret = $this->settingService->editGroup($this->post);
            $log_params['params'] = json_encode($this->post);
            $log_params['message'] = "编辑分组：{$this->ret['info']}";
            $this->saveLog($log_params);
        }elseif(IS_GET){
            $where['id'] = intval($this->req['id']);
            $group_info = $this->settingService->getGroupInfoByWhere($where);
            if(!empty($group_info)){
                $this->ret = array('status' => 1, 'info' => '查询成功', 'data' => $group_info);
            }else{
                $this->ret = array('status' => 0, 'info' => '查询失败');
            }
        }
        $this->ajaxReturn($this->ret);
    }

    public function deleteGroup(){
        $this->ret = $this->settingService->deleteGroupById($this->post['id']);
        $log_params['params'] = json_encode($this->post);
        $log_params['message'] = "删除分组状态：{$this->ret['info']}";
        $this->saveLog($log_params);
        $this->ajaxReturn($this->ret);
    }

    public function managerSetting(){
        $actions['getSettingList'] = \Url::get_function_url('setting', 'index', 'getSettingList',array(),true);
        $actions['addSetting'] = \Url::get_function_url('setting', 'index', 'addSetting',array(),true);
        $actions['saveSetting'] = \Url::get_function_url('setting', 'index', 'saveSetting',array(),true);
        $actions['deleteSetting'] = \Url::get_function_url('setting', 'index', 'deleteSetting',array(),true);
        $this->view->assign('actions' ,$actions);
        $this->view->assign('group_id' ,$this->req['group_id']);
        $this->view->display('Admin/Setting/setting.html');
    }

    public function saveSetting(){
        $this->ret = $this->settingService->saveSetting($this->post);
        $config_groupids = $this->post['config_groupid'];
        $this->clearDomainSufixCache(array_pop($config_groupids));
        $log_params['params'] = json_encode($this->post);
        $log_params['message'] = "保存设置：{$this->ret['info']}";
        $this->saveLog($log_params);
        $this->ajaxReturn($this->ret);
    }

    public function deleteSetting(){
        $where['id'] = $this->post['id'];
        $setting_info = $this->settingService->getSettingInfoByWhere($where);
        $this->ret = $this->settingService->deleteSettingById($this->post['id']);
        if(1 == $this->ret['status']){
            $this->clearDomainSufixCache($setting_info['config_groupid']);
        }
        $log_params['params'] = json_encode($this->post);
        $log_params['message'] = "删除设置：{$this->ret['info']}";
        $this->saveLog($log_params);
        $this->ajaxReturn($this->ret);
    }


    public function addSetting(){
        if(IS_POST){
            $this->ret = $this->settingService->addSetting($this->post);
            $log_params['params'] = json_encode($this->post);
            if(1 == $this->ret['status']){
                $this->clearDomainSufixCache($this->post['config_groupid']);
            }
            $log_params['message'] = "新增设置：{$this->ret['info']}";
            $this->saveLog($log_params);
        }elseif(IS_GET){
            $this->ret['status'] = 1;
            $result['group_id'] = $this->req['group_id'];
            $result['group_list'] = $this->settingService->getAllGroup();
            $this->ret['data'] = $result;
        }
        $this->ajaxReturn($this->ret);
    }

    /**
     * 清除域名后缀配置缓存
     */
    private function clearDomainSufixCache($group_id = 0){
        $cacheService = new \Service\Cache\CacheService();
        $cacheService->clearDomainSufixCacheByGroupId($group_id);
    }

}
