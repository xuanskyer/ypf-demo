<?php
namespace Model;
class Access extends \Model\CommonModel{



	public function __construct(){
		parent::__construct();
        $this->table_name = 'rbac_access';
	}

    public function get_role_node_ids($role_id = 0){
        $node_ids = array();
        $where['role_id'] = $role_id;
        $list = $this->getListByWhere($where);
        if(is_array($list) && !empty($list)){
            foreach($list as $val){
                $node_ids[] = $val['node_id'];
            }
        }
        return $node_ids;
    }
}