<?php
/**
 * Desc: 业务服务层公共类
 * Created by PhpStorm.
 * User: xuanskyer | <furthestworld@icloud.com>
 * Date: 2015/7/1 16:50
 */

namespace Service;

abstract class Service extends \Ypf\Core\Service{
	CONST PAGE_SIZE = 20;
    public $models;
    public $ret;
    public $configall;
    public function __construct(){
        parent::__construct();
        $this->configall = \Ypf\Lib\Config::$config;//config;
        $this->models = new \stdClass();
        $this->ret = array('status' => 0, 'info' => '操作失败');
    }

    //初始化服务层需要用的模型抽象方法
    abstract public function initModels();

    /**
     * 设置过期时间刷选查询条件
     * @param int $end_time_type
     * @return array
     */
    public function setEndTimeWhere($end_time_type = 0){
        $end_time_type = intval($end_time_type);
        switch($end_time_type){
            case 1: //3天内过期
                $end_time_where = array(
                    array('egt', date('Y-m-d')),
                    array('elt', date("Y-m-d",time()+60*60*24*3))
                );
                break;
            case 2: //3天后过期
                $end_time_where = array('egt', date("Y-m-d",time()+60*60*24*3));
                break;
            case 3: //已过期
                $end_time_where = array('elt', date("Y-m-d"));
                break;
            default:    //全部
                $end_time_where = array();
                break;
        }
        return $end_time_where;
    }
	
}