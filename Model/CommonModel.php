<?php
namespace Model;
class CommonModel extends \Model\Model{
	//config
	protected $configall;

	public function __construct(){
		parent::__construct();
		$this->configall = $this->config->getAll();
				
	}
	//方法未定义或不可访问时调用
	public function __call($method, $args){
		die("Model function ( $method ) not exist ");
	}
	//php 5.3.0 version later to call
	public static function __callStatic($method, $args){
		die("Model static function ( $method ) not exist ");
	}
	
}