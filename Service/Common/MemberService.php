<?php
/**
 * Desc: 会员服务类
 * Created by PhpStorm.
 * User: xuanskyer | <furthestworld@icloud.com>
 * Date: 2015/7/27
 */

namespace Service\Common;
class MemberService extends \Service\Service{

    public $ret;

    public function __construct(){
        parent::__construct();
        $this->ret = array('status' => 0, 'info' => '操作失败');
        $this->initModels();
    }


    public function initModels(){
        $this->models = new \stdClass();
        $this->models->member                       = new \Model\Member();
    }

    public function getMemberInfoById($id = 0){
        $id = intval($id);
        if($id){
            $where['id'] = $id;
            $member_info = $this->models->member->getOneByWhere($where);
            $this->ret = array(
                'status' => 1,
                'info'   => "查询域名成功",
                'data'   => $member_info
            );
        }else{
            $this->ret['info'] = '查询用户信息失败';
        }
        return $this->ret;
    }

    public function getLoginedMemberId(){
        return isset($_SESSION[$this->configall['common']['USER_AUTH_KEY']]['id'])?$_SESSION[$this->configall['common']['USER_AUTH_KEY']]['id']:null;
    }

    public function getLoginedMemberInfo(){
        return isset($_SESSION[$this->configall['common']['USER_AUTH_KEY']])?$_SESSION[$this->configall['common']['USER_AUTH_KEY']]: array();
    }

    /**
     * 判断用户是否是代理的下线
     * @param int $member_id
     * @return bool
     */
    public function isChildMember($member_id = 0){
        $is_child = false;
        if($member_id){
            $where['id'] = intval($member_id);
            $pid = $this->models->member->getOneFieldByWhere($where, 'pid');
            $pid && $is_child = true;
        }
        return $is_child;
    }

    /**
     * 判断用户是否是代理
     * @param int $member_id
     * @return bool
     */
    public function isAgentMember($member_id = 0){
        $is_agent = false;
        if($member_id){
            $where['pid'] = intval($member_id);
            $count = $this->models->member->getListCountByWhere($where);
            $count && $is_agent = true;
        }

        return $is_agent;
    }

    /**
     * 递归查询用户所属代理用户ID
     * @param int $member_id
     */
    public function getMemberAgentId($member_id = 0){
        static $loop = 0;
        static $agent_list = array();
        if($member_id && $loop <= 6){
            $loop++;
            array_push($agent_list, $member_id);
            $pid = $this->models->member->getOneFieldByWhere(array('id' => $member_id), 'pid');
            $this->getMemberAgentId($pid);
        }
        return $agent_list;
    }

    /**
     * 查询代理用户的所有下线
     * @param int $agent_member_id
     * @return array
     */
    public function getAgentChildrenIdsList($agent_member_id = 0){
        $children = array();
        if($agent_member_id){
            $where['pid'] = $agent_member_id;
            $children = $this->models->member->getListByWhere($where);
        }
        return $children;
    }



    public function getMemberListByWhere($where = array(), $offset = 0, $limit = 500, $order = array(), $fields = array()){
        $count = $this->models->member->getListCountByWhere($where);
        $data = $this->models->member->getListByWhere($where, $offset, $limit, $order);
        $this->unsetMemberPassword($data);
        $result = array(
            'count' => $count ? $count : 0,
            'data' => $count && !empty($data) ? $data : array(),
        );

        return $result;
    }
    
    public function unsetMemberPassword(&$members = array()){
        foreach($members as $key => $val){
            unset($members[$key]['password']);
        }
    }

    /**
     * 查询用户的代理关系
     * @param int $member_id
     * @return array
     */
    public function getMemberRelation($member_id = 0){
        $agent_list = $this->getMemberAgentId($member_id);
        $agent_list = array_reverse($agent_list);
        $where = array('id' => array('in', $agent_list));
        $list = $this->models->member->getListByWhere($where);
        foreach((array)$list as $key => $val){
            $member_list[$val['id']] = $val;
        }
        foreach((array)$agent_list as $key => $val){
            $relation_member[] = $member_list[$val]['email'];
        }
        return $relation_member;
    }

    public function deleteMemberById($member_id = 0){
        if($member_id){
            $where = array('pid' => $member_id);
            $count = $this->models->member->getListCountByWhere($where);
            if($count){
                $this->ret = array('status' => 0, 'info' => '用户有代理下线无法直接删除');
            }else{
                $where = array('id' => $member_id);
                $res = $this->models->member->deleteByWhere($where);
                if($res){
                    $this->ret = array('status' => 1, 'info' => '删除用户成功');
                }else{
                    $this->ret = array('status' => 0, 'info' => '删除用户失败');
                }
            }
        }else{
            $this->ret = array('status' => 0, 'info' => '用户ID不能为空');
        }
        return $this->ret;
    }

    /**
     * 验证用户是否在代理关系层级树中（是否是代理或者下线）
     * @param array $member_ids
     * @return array
     */
    public function checkMemberIsInTree($member_ids = array()){
        $result = array();
        if(is_array($member_ids) && !empty($member_ids)){
            foreach($member_ids as $id){
                $where['_string'] = " ( id = {$id} AND pid > 0 ) OR pid = {$id} ";
                $count = $this->models->member->getListCountByWhere($where);
                $result[$id] = $count ? true : false;
            }
        }
        return $result;
    }
}