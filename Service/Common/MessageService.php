<?php
/**
 * Desc: 消息发送公共服务类
 * Created by PhpStorm.
 * User: xuanskyer | <furthestworld@icloud.com>
 * Date: 2015/7/21
 */

namespace Service\Common;
class MessageService extends \Service\Service{

    public $ret;

    public function __construct(){
        parent::__construct();
        $this->ret = array('status' => 0, 'info' => '操作失败');
        $this->initModels();
    }


    public function initModels(){
        $this->models = new \stdClass();
        $this->models->member                        = new \Model\Member();
    }


}