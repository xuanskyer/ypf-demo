<?php
namespace Controller\Admin\Common;
class SlideCommon extends \Controller\Admin\Common\Common{
	public function index(){
        $args = func_get_args();
        if(is_array($args) && !empty($args)){
            foreach($args as $sub_slide){
                $slides[$sub_slide] = $this->sub_slide($sub_slide);
            }
            $this->view->assign('slides', $slides);
        }
        $slide_list = $this->get_slide_nav_nodes();
        $current_node = $this->get_current_nav_node(2);
        empty($current_node) && $current_node = strtolower(CONTROLLER_NAME);
        foreach((array)$slide_list as $key => $val){
            $node_name = $val['nav_name'] ? strtolower($val['nav_name']) : strtolower($val['name']);
            $slide_list[$key]['class'] = $current_node == $node_name ? 'active' : '';
        }
//        v(CONTROLLER_NAME);
//        v(FUNCTION_NAME);;
//        v($current_node);
//        v($slide_list);
        $this->view->assign('slide_list', $slide_list);
        $this->view->assign('current_node', $this->get_current_nav_node());


		return $this->view->fetcher('Admin/Public/slide_common.html');
	}

    public function sub_slide($sub_slide = ''){
        if($sub_slide){
            return $this->view->fetcher("Admin/Public/slide/slide_{$sub_slide}.html");
        }
    }

}