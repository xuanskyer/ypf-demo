<?php
/**
 * Desc: 自动化开发功能服务类
 * Created by PhpStorm.
 * User: xuanskyer | <furthestworld@icloud.com>
 * Date: 2016-7-18 14:37:14
 */

namespace Service\Develop;

use Service\Common\AutoCommonService;
class AutoDevModuleService extends AutoCommonService {


    public function __construct() {
        parent::__construct();
        $this->ret = array('status' => 0, 'info' => '操作失败');
        $this->initModels();
        $this->initListFieldMap();
        $this->initEditFieldMap();
        $this->initAddFieldMap();
    }

    public function initModels() {
        parent::initModels();
        $this->defaultModel = new \Model\AutoDevModule();
    }

    /**
     * @node_name 配置新增页显示字段
     */
    public function initAddFieldMap() {
        $this->addFieldMap = [
            [
                'field_name'  => 'id',
                'field_type'  => 'int',
                'field_title' => 'ID',
                'is_show'     => true,
                'form_config' => [
                    'name' => 'id',
                    'type' => 'hidden'
                ],
            ],
            [
                'field_name'  => 'name',
                'field_type'  => 'varchar',
                'field_title' => '名称',
                'is_show'     => true,
                'form_config' => [
                    'name'  => 'name',
                    'title' => '名称',
                    'type'  => 'text',
                ],
            ],
            [
                'field_name'  => 'description',
                'field_type'  => 'text',
                'field_title' => '描述',
                'is_show'     => true,
                'form_config' => [
                    'name'  => 'description',
                    'title' => '描述',
                    'type'  => 'textarea',
                ],
            ],
            [
                'field_name'  => 'created_time',
                'field_type'  => 'datetime',
                'field_title' => '新增时间',
                'is_show'     => true,
                'form_config' => [
                    'name'   => 'created_time',
                    'title'  => '新增时间',
                    'type'   => 'text',
                    'class'  => 'form-control lhgcalendar',
                    'output' => 'readonly',
                    'value' => date('Y-m-d H:i:s')
                ],
            ],
            [
                'field_name'  => 'updated_time',
                'field_type'  => 'timestamp',
                'field_title' => '更新时间',
                'is_show'     => false,
                'form_config' => [
                    'name'   => 'updated_time',
                    'title'  => '更新时间',
                    'type'   => 'text',
                    'class'  => 'form-control lhgcalendar',
                    'output' => 'readonly',
                ],
            ],
            [
                'field_name'  => 'status',
                'field_type'  => 'tinyint',
                'field_title' => '状态',
                'is_show'     => true,
                'form_config' => [
                    'name'           => 'status',
                    'title'          => '状态',
                    'type'           => 'select',
                    'select_options' => [
                        ['name' => '启用', 'value' => 1],
                        ['name' => '禁用', 'value' => 2],
                    ],
                ],
            ],
            [
                'field_name'  => 'sort',
                'field_type'  => 'int',
                'field_title' => '排序',
            ],
            [
                'field_name'  => '',
                'field_type'  => '',
                'field_title' => '操作',
                'is_show'     => true,
            ],
        ];
    }

    /**
     * @node_name 配置列表页显示字段
     */
    public function initListFieldMap() {
        $this->listFieldMap = [
            [
                'field_name'  => 'id',
                'field_type'  => 'int',
                'field_title' => 'ID',
                'is_show'     => true,
                'is_search'   => true,
                'form_config' => [
                    'name' => 'id',
                    'title' => 'ID',
                    'type' => 'text'
                ],
            ],
            [
                'field_name'  => 'name',
                'field_type'  => 'varchar',
                'field_title' => '名称',
                'is_show'     => true,
                'is_search'   => true,
                'form_config' => [
                    'name'  => 'name',
                    'title' => '名称',
                    'type'  => 'text',
                    'data-control-action' => 'filter'
                ],
            ],
            [
                'field_name'  => 'description',
                'field_type'  => 'text',
                'field_title' => '描述',
                'is_show'     => true,
                'form_config' => [
                    'name'  => 'description',
                    'title' => '描述',
                    'type'  => 'textarea',
                ],
            ],
            [
                'field_name'  => 'created_time',
                'field_type'  => 'datetime',
                'field_title' => '新增时间',
                'is_show'     => true,
                'is_search'   => true,
                'form_config' => [
                    'name'   => 'created_time',
                    'title'  => '新增时间',
                    'type'   => 'text',
                    'class'  => 'lhgcalendar',
                    'output' => 'readonly',
                    'data-control-action' => 'filterGeq'
                ],
            ],
            [
                'field_name'  => 'updated_time',
                'field_type'  => 'timestamp',
                'field_title' => '更新时间',
                'is_show'     => true,
                'is_search'   => true,
                'form_config' => [
                    'name'   => 'updated_time',
                    'title'  => '更新时间',
                    'type'   => 'text',
                    'class'  => 'lhgcalendar',
                    'output' => 'readonly',
                    'data-control-action' => 'filterLeq'
                ],
            ],
            [
                'field_name'  => 'status',
                'field_type'  => 'tinyint',
                'field_title' => '状态',
                'is_show'     => true,
                'is_search'   => true,
                'form_config' => [
                    'name'           => 'status',
                    'title'          => '状态',
                    'type'           => 'select',
                    'select_options' => [
                        ['name' => '启用', 'value' => 1],
                        ['name' => '禁用', 'value' => 2],
                    ],
                ],
            ],
            [
                'field_name'  => 'sort',
                'field_type'  => 'int',
                'field_title' => '排序',
                'is_show'     => true,
            ],
            [
                'field_name'  => '',
                'field_type'  => '',
                'field_title' => '操作',
                'is_show'     => true,
            ],
        ];
    }

    /**
     * @node_name 配置编辑也显示字段
     */
    public function initEditFieldMap() {
        $this->editFieldMap = [
            [
                'field_name'  => 'id',
                'field_type'  => 'int',
                'field_title' => 'ID',
                'is_show'     => true,
                'form_config' => [
                    'name' => 'id',
                    'type' => 'hidden'
                ],
            ],
            [
                'field_name'  => 'name',
                'field_type'  => 'varchar',
                'field_title' => '名称',
                'is_show'     => true,
                'form_config' => [
                    'name'  => 'name',
                    'title' => '名称',
                    'type'  => 'text',
                ],
            ],
            [
                'field_name'  => 'description',
                'field_type'  => 'text',
                'field_title' => '描述',
                'is_show'     => true,
                'form_config' => [
                    'name'  => 'description',
                    'title' => '描述',
                    'type'  => 'textarea',
                ],
            ],
            [
                'field_name'  => 'created_time',
                'field_type'  => 'datetime',
                'field_title' => '新增时间',
                'is_show'     => true,
                'form_config' => [
                    'name'   => 'created_time',
                    'title'  => '新增时间',
                    'type'   => 'text',
                    'class'  => 'form-control lhgcalendar',
                    'output' => 'readonly',
                ],
            ],
            [
                'field_name'  => 'updated_time',
                'field_type'  => 'timestamp',
                'field_title' => '更新时间',
                'is_show'     => false,
                'form_config' => [
                    'name'   => 'updated_time',
                    'title'  => '更新时间',
                    'type'   => 'text',
                    'class'  => 'form-control lhgcalendar',
                    'output' => 'readonly',
                ],
            ],
            [
                'field_name'  => 'status',
                'field_type'  => 'tinyint',
                'field_title' => '状态',
                'is_show'     => true,
                'form_config' => [
                    'name'           => 'status',
                    'title'          => '状态',
                    'type'           => 'select',
                    'select_options' => [
                        ['name' => '启用', 'value' => 1],
                        ['name' => '禁用', 'value' => 2],
                    ],
                ],
            ],
            [
                'field_name'  => 'sort',
                'field_type'  => 'int',
                'field_title' => '排序',
            ],
            [
                'field_name'  => '',
                'field_type'  => '',
                'field_title' => '操作',
                'is_show'     => true,
            ],
        ];
    }

    public function getTableFields($params = []){
        $ret = ['status' => 0, 'info' => '查询字段失败'];
        if(!isset($params['db_name']) && empty($params['db_name'])){
            return ['status' => 0, 'info' => '请选择数据库'];
        }
        if(!isset($this->configall['db'][$params['db_name']]['dbname'])){
            return ['status' => 0, 'info' => '未查询到数据库配置'];
        }
        $db_name = $this->configall['db'][$params['db_name']]['dbname'];

        if(isset($params['table_name']) && !empty($params['table_name'])){
            $res = $this->defaultModel->getTableFields("{$db_name}.{$params['table_name']}");
            if($res){
                foreach($res as $key => $one){
                    $res[$key]['form_value'] = base64_encode(serialize($one));
                }
                return ['status' => 1, 'info' => '查询字段成功', 'data' => $res];
            }else{
                return ['status' => 0, 'info' => '查询字段失败', 'data' => $res];
            }
        }else{
            $ret['info'] = '请填写数据表名称';
        }
        return $ret;
    }

}