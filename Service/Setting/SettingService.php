<?php
/**
 * Desc: 配置管理服务类
 * Created by PhpStorm.
 * User: xuanskyer | <furthestworld@icloud.com>
 * Date: 2015/9/14 11:11
 */

namespace Service\Setting;
class SettingService extends \Service\Service{

    const LOGIN_HOME_SETTING    =   'login_home_domain';
    public $ret;

    public $status_switch = array(
        'on'    =>  1,
        'off'   =>  0
    );
    public function __construct(){
        parent::__construct();
        $this->ret = array('status' => 0, 'info' => '操作失败');
        $this->initModels();
    }


    public function initModels(){
        $this->models = new \stdClass();
        $this->models->admin_setting                        = new \Model\AdminSetting();
        $this->models->admin_setting_group                        = new \Model\AdminSettingGroup();
    }

    public function getGroupList($params = array()){
        $result = array(
            'count'     =>  0,
            'data'      => array()
        );
        extract($params);
        empty($where) && $where = array();
        empty($offset) && $offset = 0;
        empty($limit) && $limit = 20;
        empty($order) && $order = array();
        $result['count'] = $this->models->admin_setting_group->getListCountByWhere($where);
        $result['data'] = $this->models->admin_setting_group->getListByWhere($where, $offset, $limit, $order);
        return $result;
    }

    public function getAllGroup(){
        return $this->models->admin_setting_group->getListByWhere();
    }

    public function getSettingList($params = array()){
        $result = array(
            'count'     =>  0,
            'data'      => array()
        );
        extract($params);
        empty($where) && $where = array();
        empty($offset) && $offset = 0;
        empty($limit) && $limit = 20;
        empty($order) && $order = array();
        $result['count'] = $this->models->admin_setting->getListCountByWhere($where);
        $result['data'] = $this->models->admin_setting->getListByWhere($where, $offset, $limit, $order);
        return $result;
    }

    /**
     * 新增配置分组
     * @param array $params
     * @return array
     */
    public function addGroup($params = array()){
        if(!isset($params['name']) || empty($params['name'])){
            return array('status' => 0, 'info' => '配置分组名不能为空');
        }

        if(!isset($params['groupkey']) || empty($params['groupkey'])){
            return array('status' => 0, 'info' => '配置分组键名不能为空');
        }elseif( $this->checkGroupKeyExisted($params['groupkey'])){
            return array('status' => 0, 'info' => '当前分组键名已经存在');
        }
        if(!isset($params['status']) || !isset($this->status_switch[$params['status']])){
            $data['status'] = $this->status_switch['off'];
        }else{
            $data['status'] = $this->status_switch[$params['status']];
        }
        $data['name'] = trim($params['name']);
        $data['groupkey'] = trim($params['groupkey']);
        !empty($params['description']) && $data['description'] = $params['description'];
        $data['created_time'] = $data['updated_time'] = date('Y-m-d H:i:s', time());
        $res = $this->models->admin_setting_group->add($data);
        if($res){
            $this->ret = array('status' => 1, 'info' => '新增配置分组成功');
        }else{
            $this->ret = array('status' => 0, 'info' => '新增配置分组失败');
        }
        return $this->ret;
    }

    /**
     * 新增配置分组
     * @param array $params
     * @return array
     */
    public function editGroup($params = array()){
        if(!isset($params['id']) || empty($params['id'])){
            return array('status' => 0, 'info' => '未指定配置分组');
        }
        if(!isset($params['name']) || empty($params['name'])){
            return array('status' => 0, 'info' => '配置分组名不能为空');
        }

        $where['id'] = array('not in', $params['id']);
        $where['name'] = $params['name'];
        $count = $this->models->admin_setting_group->getListCountByWhere($where);
        if($count){
            return array('status' => 0, 'info' => '分组名已经存在');
        }
        if(!isset($params['groupkey']) || empty($params['groupkey'])){
            return array('status' => 0, 'info' => '配置分组键名不能为空');
        }elseif($this->checkGroupKeyExisted($params['groupkey'], array('id' => array('not in', $params['id'])))){
            return array('status' => 0, 'info' => '当前分组键名已经存在!');
        }
        if(!isset($params['status']) || !isset($this->status_switch[$params['status']])){
            $data['status'] = $this->status_switch['off'];
        }else{
            $data['status'] = $this->status_switch[$params['status']];
        }
        $data['name'] = trim($params['name']);
        $data['groupkey'] = trim($params['groupkey']);
        !empty($params['description']) && $data['description'] = $params['description'];
        $res = $this->models->admin_setting_group->updateById($params['id'], $data);
        if($res){
            $this->ret = array('status' => 1, 'info' => '更新配置分组成功');
        }else{
            $this->ret = array('status' => 0, 'info' => '更新配置分组失败');
        }
        return $this->ret;
    }

    public function deleteGroupById($group_id = 0){
        if($group_id){
            $where['config_groupid'] = $group_id;
            $count = $this->models->admin_setting->getListCountByWhere($where);
            if($count){
                return array('status' => 0, 'info' => '分组下存在配置项，无法删除');
            }
            $where = array('id' => $group_id);
            $res = $this->models->admin_setting_group->deleteByWhere($where);
            if($res){
                $this->ret = array('status' => 1, 'info' => '删除配置分组成功');
            }else{
                $this->ret = array('status' => 0, 'info' => '删除配置分组失败');
            }
        }else{
            $this->ret = array('status' => 0, 'info' => '未指定分组');
        }
        return $this->ret;
    }

    public function getGroupSettingListByGroupKey($group_key = '', $status = 1){
        if(empty($group_key)){
            $this->ret = array('status' => 0 , 'info' => '分组KEY不能为空');
            return $this->ret;
        }
        $where = array('groupkey' => $group_key, 'status' => $status);
        $group_info = $this->getGroupInfoByWhere($where);
        if(empty($group_info)){
            $this->ret = array('status' => 0 , 'info' => '分组不存在');
            return $this->ret;
        }
        $where = array('config_groupid' => $group_info['id']);
        $setting_list = $this->models->admin_setting->getListByWhere($where);
        $this->ret = array('status' => 1, 'info' => '查询成功', 'data' => $setting_list);
        return $this->ret;
    }

    public function getGroupInfoByWhere($where = array()){
        return $this->models->admin_setting_group->getOneByWhere($where);
    }

    public function getSettingInfoByWhere($where = array()){
        return $this->models->admin_setting->getOneByWhere($where);
    }

    public function saveSetting($params = array()){
        if(isset($params['ids']) && is_array($params['ids'])){
            $success_count = 0;
            foreach((array)$params['ids'] as $key => $id){
                if(!isset($params['config_value'][$key]) ) {
                    $config_value = 'false';
                }elseif(isset($params['config_value'][$key]) && 'on' == $params['config_value'][$key]){
                    $config_value = 'true';
                }else{
                    $config_value = $params['config_value'][$key];
                }
                $data = array(
                    'config_groupid' => isset($params['config_groupid'][$key]) ? $params['config_groupid'][$key] : 0,
                    'config_value'  => $config_value
                );
                $res = $this->models->admin_setting->updateById($key, $data);
                $res !== false && $success_count++;
            }
            if(count($params['ids']) == $success_count){
                $this->ret = array('status' => 1, 'info' => '编辑成功');
            }else{
                $this->ret = array('status' => 0, 'info' => '部分编辑失败');
            }
        }else{
            $this->ret = array('status' => 0, 'info' => '参数非法');
        }

        return $this->ret;
    }


    public function deleteSettingById($id = 0){
        if($id){
            $where = array('id' => $id);
            $res = $this->models->admin_setting->deleteByWhere($where);
            if($res){
                $this->ret = array('status' => 1, 'info' => '删除配置成功');
            }else{
                $this->ret = array('status' => 0, 'info' => '删除配置失败');
            }
        }else{
            $this->ret = array('status' => 0, 'info' => '未选择配置');
        }
        return $this->ret;
    }

    public function addSetting($params = array()){
        if(!isset($params['config_title']) || empty($params['config_title'])){
            return array('status' => 0, 'info' => '配置名称不能为空');
        }
        if(!isset($params['config_key']) || empty($params['config_key'])){
            return array('status' => 0, 'info' => '配置key不能为空');
        }
        if(!isset($params['config_value']) || empty($params['config_value'])){
            return array('status' => 0, 'info' => '配置value不能为空');
        }
        $where = array('config_key' => $params['config_key']);
        $count = $this->models->admin_setting->getListCountByWhere($where);
        if($count){
            return array('status' => 0, 'info' => '配置key已经存在');
        }
        $data['config_title'] = $params['config_title'];
        $data['config_key'] = $params['config_key'];
        $data['config_value'] = $params['config_value'];
        !empty($params['description']) && $data['config_description'] = $params['config_description'];
        $data['config_groupid'] = intval($params['config_groupid']);
        $data['created_time'] = $data['updated_time'] = date('Y-m-d H:i:s', time());
        $res = $this->models->admin_setting->add($data);
        if($res){
            $this->ret = array('status' => 1, 'info' => '新增配置成功');
        }else{
            $this->ret = array('status' => 0, 'info' => '新增配置失败');
        }
        return $this->ret;
    }

    public function getLoginHomeSetting(){
        $where['config_key'] = self::LOGIN_HOME_SETTING;
        $setting_info = $this->models->admin_setting->getOneByWhere($where);
        $login_home_setting = isset($setting_info['config_value']) ? $setting_info['config_value'] : '';
        return $login_home_setting;
    }

    /**
     * 查询分组键名是否存在
     * @param string $groupkey
     * @param array $where
     * @return bool
     */
    private function checkGroupKeyExisted($groupkey = '', $where = array()){
        $existed = false;
        if($groupkey){
            $where['groupkey'] = trim($groupkey);
            $count = $this->models->admin_setting_group->getListCountByWhere($where);
            $count && $existed = true;
        }
        return $existed;
    }
}