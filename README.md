# 自动化模块开发 后台管理系统
======

这是一个使用以[ypf](https://git.oschina.net/xuanskyer/ypf.git)框架的后台演示系统

登录演示账号:xuanskyer

登录演示密码:123456

## 依赖

* PDO

php的PDO扩展

* Ypf扩展

系统以php扩展Ypf作为php框架,请先安装扩展 [Ypf](https://git.oschina.net/xuanskyer/ypf.git)

* 若使用缓存,memcache扩展需要安装

## 部署

* 导入数据库文件 `/Data/ypf_demo_2016-11-06.sql`

* 设置数据库配置文件 `/Conf/db.conf`

* 配置虚拟主机

## 系统截图

![列表管理](https://git.oschina.net/xuanskyer/ypf-demo/attach_files/download?i=69433&u=http%3A%2F%2Ffiles.git.oschina.net%2Fgroup1%2FM00%2F00%2F9C%2FPaAvDFgxHlSAE1qGAAZX7YXbdD4569.png%3Ftoken%3Db0105b0f831596f3a12e303cd311f61e%26ts%3D1479614010%26attname%3D1.png)