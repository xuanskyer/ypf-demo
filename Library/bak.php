	//###############分词法########################
	public $tags_num = 100; //分词数量
	
	//扫描违规关键词
	public function hasViolationWord(){
		$body = $this->getBody();
		if(!$body) return false;
		$words = file($this->violationWord_url);
		$tags = $this->get_tags_arr($body, $this->tags_num); //分词
		$res = $this->bloomFitler($words, $tags, count($words));//过滤
		if($res && in_array(true, $res)){
			return true;
		}
		return false;
	}
	
	//bloom filter
	protected function bloomFitler($words, $tags, $entries_max = 100){
		require __LIBRARY__.'/BloomFilter/Bloom.php';
		$bloom = new \Bloom(array(
			'entries_max' => $entries_max
		));
		$bloom->set($words); //库
		$res = $bloom->has($tags);
		return $res;
	}
	
	//分词
	protected function get_tags_arr($text, $tops = 50){
		require __LIBRARY__.'/participle/pscws4.class.php';
		$pscws = new \PSCWS4();
		$pscws->set_dict(__LIBRARY__.'/participle/scws/dict.utf8.xdb');
		$pscws->set_rule(__LIBRARY__.'/participle/scws/rules.utf8.ini');
		$pscws->set_ignore(true);
		$pscws->send_text($text);
		$words = $pscws->get_tops($tops);
		$tags = array();
		foreach ($words as $val) {
		    $tags[] = $val['word'];
		}
		$pscws->close();
		return $tags;
	}
	//#################分词法end#######################################
	
	index.php
//alipay
/*
$configall = $config->getAll();
$alipay_config = $configall['ALIPAY_CONFIG'];
$load->library('/Alipay/Alipay', $alipay_config, 'alipay');
*/