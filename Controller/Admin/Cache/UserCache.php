<?php
namespace Controller\Admin\Cache;
class UserCache extends \Controller\Admin\Common\Common{

    public $settingService;
    public $statusService;
    public function __construct(){
        parent::__construct();
        $this->setHeaderFooter();

        $this->userCacheService = new \Service\Cache\UserCacheService();
    }

    public function index(){

        $this->view->assign('title' ,'用户缓存管理');
        $actions['getUserCacheList'] = \Url::get_function_url('cache', 'usercache', 'getUserCacheList',array(),true);
        $actions['clearUserCache'] = \Url::get_function_url('cache', 'usercache', 'clearUserCache',array(),true);
        $actions['batchClearUserCache'] = \Url::get_function_url('cache', 'usercache', 'batchClearUserCache',array(),true);
        $this->view->assign('actions' ,$actions);
        $this->view->display('Admin/Cache/usercache/index.html');
    }

    public function getUserCacheList(){
        $params = $this->parseJplistStatuses($this->req['statuses']);
        $params['order'] = array('id' => 'desc');
        $this->ret = $this->userCacheService->getUserCacheList($params);
        $this->ajaxReturn($this->ret);
    }

    /**
     * 清除缓存
     */
    public function clearUserCache(){
        $params = $this->post;
        $this->ret = $this->userCacheService->clearUserCache($params);
        $log_params['params'] = json_encode($this->post);
        $log_params['message'] = "清除缓存：{$this->ret['info']}";
        $this->saveLog($log_params);
        $this->ajaxReturn($this->ret);
    }

    /**
     * 批量清除缓存
     */
    public function batchClearUserCache(){
        $params = $this->post;
        $this->ret = $this->userCacheService->batchClearUserCache($params);
        $log_params['params'] = json_encode($this->post);
        $log_params['message'] = "批量清除缓存：{$this->ret['info']}";
        $this->saveLog($log_params);
        $this->ajaxReturn($this->ret);
    }

}
