<?php
/**
 * Desc: echart服务类
 * Created by PhpStorm.
 * User: xuanskyer | <furthestworld@icloud.com>
 * Date: 2016-7-5 14:32:08
 */

namespace Service\EChart;

class EChartBarService extends EChartService {

    static public $bar_option;

    static public function init(){
        self::$bar_option = [
            'title'      => [
                'text'    => '',
                'subtext' => ''
            ],
            'tooltip'    => [
                'trigger' => 'axis'
            ],
            'grid' => [],
            'legend'     => [
                'data' => []
            ],
            'toolbox'    => [
                'show'    => true,
                'feature' => []
            ],
            'calculable' => true,
            'xAxis'      => [
//                [
//                    'type'        => 'value',
//                    'boundaryGap' => [0, 0.01]
//                ]
            ],
            'yAxis'      => [
//                [
//                    'type' => '',
//                    'data' => []
//                ]
            ],
            'series'     => [
//                [
//                    'name' => '',
//                    'type' => '',
//                    'data' => []
//                ],
            ],
        ];
    }

    /**
     * @node_name 格式化列表数据为bar图表格式数据
     *
     * @param array $list
     * @return array
     *
     */
    static public function formatList2BarData($list = []) {


        $yAxis_data  = [
            'type' => 'category',
            'data' => array_column($list, 'key')
        ];
        $series_data = [
            'name' => '攻击IP',
            'type' => 'bar',
            'data' => array_column($list, 'doc_count')
        ];
        array_push(self::$bar_option['yAxis'], $yAxis_data);
        array_push(self::$bar_option['series'], $series_data);
        return self::$bar_option;
    }

    static function setBarOptionTitle($params = [
        'text'    => '',
        'subtext' => ''
    ]) {
        self::$bar_option['title']['text']    = $params['text'];
        self::$bar_option['title']['subtext'] = $params['subtext'];
    }

    static function setBarOptionTooltip($params = [
        'trigger' => 'axis'
    ]) {
        self::$bar_option['tooltip']['trigger'] = $params['trigger'];
    }

    static function setBarOptionLegend($params = [
        'data' => []
    ]) {
        array_push(self::$bar_option['legend']['data'], $params['data']);
    }

    static function setBarOptionToolbox($params = [
        'show'    => true,
        'feature' => []
    ]) {
        self::$bar_option['toolbox']['show']    = $params['show'];
        self::$bar_option['toolbox']['feature'] = $params['feature'];
    }

    static function setBarOptionGrid($grid = []) {
        foreach($grid as $key => $val){
            $grid_data[$key] = $val;
        }
        self::$bar_option['grid'] = $grid_data;
    }

    static function setBarOptionCalculable($calculable = true) {
        self::$bar_option['calculable'] = $calculable;
    }

    static function setBarOptionXAxis($params = [
        'type'        => 'value',
        'boundaryGap' => [0, 0.01]
    ]) {
        foreach($params as $key => $val){
            $xAxis_data[$key] = $val;
        }
        array_push(self::$bar_option['xAxis'], $xAxis_data);
    }

    static function setBarOptionYAxis($params = [
        'type' => '',
        'data' => []
    ]) {
        foreach($params as $key => $val){
            $yAxis_data[$key] = $val;
        }
        array_push(self::$bar_option['yAxis'], $yAxis_data);
    }

    static function setBarOptionSeries($params = [
        'name' => '',
        'type' => '',
        'data' => []
    ]) {
        $series_data = [
            'name' => $params['name'],
            'type' => $params['type'],
            'data' => $params['data']
        ];
        array_push(self::$bar_option['series'], $series_data);
    }

    static function getBarOption(){
        return self::$bar_option;
    }
}