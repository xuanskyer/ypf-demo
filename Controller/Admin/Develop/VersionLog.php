<?php
/**
 * 后台系统功能更新日志
 */
namespace Controller\Admin\Develop;
class VersionLog extends \Controller\Admin\Common\Common{

	public $versionLogService;
	public function __construct(){
		parent::__construct();
		$this->setHeaderFooter();
		$this->versionLogService = new \Service\Develop\VersionLogService();
	}
	
	public function index(){
		$actions['getVersionLogList'] = \Url::get_function_url('develop', 'versionlog', 'getVersionLogList',array(),true);
		$actions['add'] = \Url::get_function_url('develop', 'versionlog', 'add',array(),true);
		$actions['edit'] = \Url::get_function_url('develop', 'versionlog', 'edit',array(),true);
		$actions['delete'] = \Url::get_function_url('develop', 'versionlog', 'delete',array(),true);
		$actions['batchDelete'] = \Url::get_function_url('develop', 'versionlog', 'batchDelete',array(),true);
		$this->view->assign('actions', $actions);
		$this->view->assign('title', '系统更新日志');
		$this->view->display('Admin/Develop/VersionLog/index.html');
	}

	public function getVersionLogList(){
		$params = $this->parseJplistStatuses($this->req['statuses']);
		$this->ret = $this->versionLogService->getVersionLogList($params);
		$this->ajaxReturn($this->ret);
	}

	public function add(){
		if(IS_POST){
			$this->ret = $this->versionLogService->add($this->post);
			$log_params['params'] = json_encode($this->post);
			$log_params['message'] = "新增系统更新日志：{$this->ret['info']}";
			$this->saveLog($log_params);
		}else{
			$actions['add'] = \Url::get_function_url('develop', 'versionlog', 'add',array(),true);
			$data['actions'] = $actions;
			$this->ret = array(
				'status' => 1,
				'info'	=> '查询成功',
				'data' => $data
			);
		}

		$this->ajaxReturn($this->ret);
	}

	public function edit(){
		if(IS_POST){
			$this->ret = $this->versionLogService->edit($this->post);
			$log_params['params'] = json_encode($this->post);
			$log_params['message'] = "编辑系统更新日志：{$this->ret['info']}";
			$this->saveLog($log_params);
		}else{
			$actions['edit'] = \Url::get_function_url('develop', 'versionlog', 'edit',array(),true);
			$data['actions'] = $actions;
			$res = $this->versionLogService->getVersionLogInfoByWhere(array('id' => $this->req['id']));
			if(1 == $res['status']){
				$data['log_info'] = $res['data'];
			}
			$this->ret = array(
				'status' => $res['status'],
				'info'	=> '查询成功',
				'data' => $data
			);
		}
		$this->ajaxReturn($this->ret);
	}

	public function delete(){
		$this->ret = $this->versionLogService->delete($this->post);
		$log_params['params'] = json_encode($this->post);
		$log_params['message'] = "删除系统更新日志：{$this->ret['info']}";
		$this->saveLog($log_params);
		$this->ajaxReturn($this->ret);
	}

	public function batchDelete(){
		$this->ret = $this->versionLogService->batchDelete($this->post);
		$log_params['params'] = json_encode($this->post);
		$log_params['message'] = "批量删除系统更新日志：{$this->ret['info']}";
		$this->saveLog($log_params);
		$this->ajaxReturn($this->ret);
	}
	
}