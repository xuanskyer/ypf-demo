<?php
/**
 * Desc: 后台系统功能更新日志服务类
 * Created by PhpStorm.
 * User: xuanskyer | <furthestworld@icloud.com>
 * Date: 2015-12-2 14:30:47
 */

namespace Service\Develop;
class VersionLogService extends \Service\Service{

    const STATUS_OK         =   1;
    const STATUS_FORBID     =   0;
    const STATUS_DELETE     =   -1;
    public $status_list;
    public function __construct(){
        parent::__construct();
        $this->initModels();
        $this->status_list = array(
            self::STATUS_OK     =>  '正常',
            self::STATUS_FORBID =>  '禁用',
            self::STATUS_DELETE =>  '删除'
        );
    }

    public function initModels(){
        $this->models = new \stdClass();
        $this->models->version_log                   = new \Model\AdminVersionLog();
    }

    public function getVersionLogList($params = array()){

        $result = array(
            'count'     =>  0,
            'data'      => array()
        );
        extract($params);
        empty($where) && $where = array();
        empty($offset) && $offset = 0;
        empty($limit) && $limit = self::PAGE_SIZE;
        empty($order) && $order = array();
        if(isset($where['`create_time`'])){
            $expiry_time = trim($where['`create_time`'][1], '%');
            if(!empty($expiry_time)){
                $where['`create_time`'] = array('lt', $expiry_time);
                $order = array('`create_time`' => 'desc');
            }else{
                unset($where['`create_time`']);
            }
        }
        if(isset($where['`update_time`'])){
            $expiry_time = trim($where['`update_time`'][1], '%');
            if(!empty($expiry_time)){
                $where['`update_time`'] = array('lt', $expiry_time);
                $order = array('`update_time`' => 'desc');
            }else{
                unset($where['`update_time`']);
            }
        }
        $where['status'] = array('in', array(self::STATUS_OK, self::STATUS_FORBID));
        $total = $this->models->version_log->getListCountByWhere($where);
        if($total){
            $list = $this->models->version_log->getListByWhere($where, $offset, $limit, $order, $fields);
            $data['data_list'] = $list;
            $data['status_list'] = $this->status_list;
            $result = array(
                'count' => $total,
                'data'  => $data
            );
        }

        return $result;
    }

    public function add($params = array()){
        if(is_array($params) && !empty($params)){
            $data['title'] = trim($params['title']);
            $data['content'] = trim($params['content']);
            $data['create_time'] = $data['update_time'] = date('Y-m-d H:i:s', time());
            $data['status'] = self::STATUS_OK;
            $res = $this->models->version_log->add($data);
            if($res){
                $this->ret = array('status' => 1, 'info' => '新增成功');
            }else{
                $this->ret = array('status' => 0, 'info' => '新增失败');
            }
        }else{
            $this->ret = array('status' => 0, 'info' => '缺少参数');
        }
        return $this->ret;
    }

    public function edit($params = array()){
        if(is_array($params) && !empty($params)){
            if(!isset($params['id'])){
                return array('status' => 0, 'info' => '未指定编辑记录');
            }
            $where['id'] = intval($params['id']);
            $data['title'] = trim($params['title']);
            $data['content'] = trim($params['content']);
            $res = $this->models->version_log->updateByWhere($where, $data);
            if($res){
                $this->ret = array('status' => 1, 'info' => '编辑成功');
            }else{
                $this->ret = array('status' => 0, 'info' => '编辑失败');
            }
        }else{
            $this->ret = array('status' => 0, 'info' => '缺少参数');
        }
        return $this->ret;
    }

    /**
     * 逻辑删除
     * @param array $params
     * @return array
     */
    public function delete($params = array()){
        if(!isset($params['id'])){
            $this->ret = array('status' => 0, 'info' => '未指定删除记录ID');
        }else{
            $where['id'] = $params['id'];
            $data['status'] = self::STATUS_DELETE;
            $res = $this->models->version_log->updateByWhere($where, $data);
            if($res){
                $this->ret = array('status' => 1, 'info' => '删除成功');
            }else{
                $this->ret = array('status' => 0, 'info' => '删除失败');
            }
        }
        return $this->ret;
    }

    public function batchDelete($params = array()){
        if(!isset($params['batch_ids']) || empty($params['batch_ids'])){
            $this->ret = array('status' => 0, 'info' => '请选择删除记录');
        }else{
            $where['id'] = array('in', $params['batch_ids']);
            $data['status'] = self::STATUS_DELETE;
            $res = $this->models->version_log->updateByWhere($where, $data);
            if($res){
                $this->ret = array('status' => 1, 'info' => '删除成功');
            }else{
                $this->ret = array('status' => 0, 'info' => '删除失败');
            }
        }
        return $this->ret;
    }

    public function getVersionLogInfoByWhere($where = array()){
        if(!empty($where)){
            $info = $this->models->version_log->getOneByWhere($where);
        }
        if($info){
            $this->ret = array('status' => 1, 'info' => '查询日志成功', 'data' => $info);
        }else{
            $this->ret = array('status' => 0, 'info' => '查询日志失败');
        }
        return $this->ret;
    }

    public function getLatestVersionLog(){
        $where['status'] = self::STATUS_OK;
        $limit = 20;
        $order = array('id' => 'desc');
        $list = $this->models->version_log->getListByWhere($where, 0, $limit, $order);
        if($list){
            $this->ret = array('status' => 1, 'info' => '查询日志成功', 'data' => $list);
        }else{
            $this->ret = array('status' => 0, 'info' => '查询日志失败');
        }
        return $this->ret;
    }

}