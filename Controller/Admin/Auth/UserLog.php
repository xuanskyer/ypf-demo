<?php
namespace Controller\Admin\Auth;
class UserLog extends \Controller\Admin\Common\Common{
	
	private $m_log = null;
	public $userLogService;
	
	public function __construct(){
		parent::__construct();
		$this->m_log = new \Model\AdminOperateLog();
		$this->userLogService = new \Service\Auth\UserLogService();
	}
	
	public function index(){
		$this->setHeaderFooter();
		$actions['exportAuthLog'] = $this->buildUrl('exportAuthLog');
		$this->view->assign('actions' ,$actions);
		$this->view->display('Admin/Auth/userlog/index.html');
	}
	
	public function operate(){
		$op = isset($this->req['op']) ? trim($this->req['op']) : 'list';
		$return = array(
				'status' => 0,
				'info' => '操作失败'
		);
		switch ($op){
			case 'list':
				$param = $this->parseJplistStatuses($this->req['statuses']);
				$r = $this->m_log->getListByWhere($param['where'], $param['offset'], $param['limit'], $param['order']);
				$array = array(
						'count' => $this->m_log->getListCountByWhere($param['where']),
						'data' => $r
				);
				$this->ajaxReturn($array);
				break;
			case 'exportAuthLog':
				$this->exportAuthLog($this->post);
				break;
				
			default:break;
		}
	}

	public function exportAuthLog(){
		$this->userLogService->exportAuthLog($this->post);
	}
	
}