<?php
namespace Model;
class Operation extends \Model\CommonModel{
	
	public function __construct(){
		parent::__construct();
	}
	
	//获取服务器ip列表
	public function getServerIpList($page=1, $pagesize=20){
		$sql_urgent = 'SELECT 
				* 
				from 
				'.$this->tbl_server_ip.' 
				where 
				`pid`=0 AND `end_time`>="'.date('Y-m-d').'" 
				AND `end_time`<="'.date("Y-m-d",time()+60*60*24*3).'" 
				ORDER BY `end_time` asc';
		$res_urgent = $this->db->select($sql_urgent);
		$res_urgent = $res_urgent ? $res_urgent : array();//三天内即将到期
		
		$sql_normal = 'SELECT
				* 
				from
				'.$this->tbl_server_ip.'
				WHERE 
				`pid`=0 AND `end_time`>="'.date("Y-m-d",time()+60*60*24*3).'"
				ORDER BY `end_time` asc';
		$res_normal = $this->db->select($sql_normal);
		$res_normal = $res_normal ? $res_normal : array();
		
		$sql_over = 'SELECT
				* 
				from
				'.$this->tbl_server_ip.'
				WHERE 
				`pid`=0 AND `end_time`<"'.date("Y-m-d").'"
				ORDER BY `end_time` asc';
		$res_over = $this->db->select($sql_over);
		$res_over = $res_over ? $res_over : array();//已到期
		
		$res = array_merge($res_urgent, $res_normal, $res_over);
		$res = array_slice($res, ($page-1)*$pagesize, $pagesize);
		return $res;
	}
	
	//搜索服务器
	public function getServerIpListByWhere($where, $page = 1, $count = false){
		if (empty($where)){
			return $this->getServerIpList();
		}
		$data = ' WHERE ';
		if ($where['ip'])$temp = $data .= ' `ip`="'.$where['ip'].'" AND ';
		if ($where['location'])$data .= ' `location` like "%'.$where['location'].'%" AND ';
		if ($where['end_time'])$data .= ' `end_time`>="'.$where['end_time'].'" AND';
		if ($where['childip']){
			$data .= ' `ip`="'.$where['childip'].'" AND ';
			$data .= ' `pid`>0';
		}else{
			$data .= ' `pid`=0';
		}
		$offset = ($page-1)*20;
		$limit = ' limit '.$offset.',20';
		$sql = 'SELECT * from '.$this->tbl_server_ip.$data.$limit;
		$sql_count = 'SELECT count(*) as total from '.$this->tbl_server_ip.$data;
		if ($count){
			$res = $this->db->select($sql_count);
			$res = $res[0]['total'] ? $res[0]['total'] : 0;
		}else{
			$res = $this->db->select($sql);
		}
		return $res;
	}
	
	//获取服务器ip的总条目
	public function getServerIpCount(){
		$r = $this->db->table($this->tbl_server_ip)->field('count(*) as total')->fetch();
		return $r['total'] ? $r['total'] : 0;
	}
	
	//获取服务器主IP的总条目
	public function getServerMainIpCount(){
		$r = $this->db->table($this->tbl_server_ip)->field('count(*) as total')->where('`pid`=0')->fetch();
		return $r['total'] ? $r['total'] : 0;
	}
	
	//获取服务器的属性 根据pid
	public function getServerAttrBypid($pid){
		$r = $this->db->table($this->tbl_attribute)->where('`type`="server" AND pid=?', array($pid))->fetch();
		return $r ? $r : array();
	}
	
	//获取服务器的信息 根据pid
	public function getServerInfoBypid($pid, $field = 'ip'){
		$r = $this->db->table($this->tbl_server_ip)->where('pid=?', array($pid))->field($field)->select();
		return $r ? $r : array();
	}
	
	//获取服务器的信息 根据pid=0 和 id
	public function getServerInfoByPid0AndId($id){
		$r = $this->db->table($this->tbl_server_ip)->where('`pid`=0 and id=?', array($id))->fetch();
		return $r ? $r : array();
	}
	
	//获取服务器信息  根据ip
	public function getServerInfoByIp($ip){
		$r = $this->db->table($this->tbl_server_ip)->where('`ip`=?', array($ip))->fetch();
		return $r ? $r : array();
	}
	
	//获取ip的信息 根据id
	public function getIpInfoById($id, $field='*'){
		$r = $this->db->table($this->tbl_server_ip)->where('id=?', array($id))->field($field)->fetch();
		return $r ? $r : array();
	}
	
	//获取服务器的属性  根据pid
	public function getServerInfoByServerAndPid($pid){
		$r = $this->db->table($this->tbl_attribute)->where('`type`="server" AND `pid`=?', array($pid))->fetch();
		return $r ? $r : array();
	}
	
	//更新服务器的属性 根据pid
	public function updateServerAttrByPid($pid, $data){
		$r = $this->db->table($this->tbl_attribute)->where('`type`="server" AND `pid`=?', array($pid))->update($data);
		return $r;
	}
	
	//更新服务器的信息 根据 id
	public function updateServerInfoByid($id, $data){
		return $this->db->table($this->tbl_server_ip)->where('id=?', array($id))->update($data);
	}
	
	//增加一条服务器ip
	public function addServerInfo($data){
		return $this->db->table($this->tbl_server_ip)->insert($data);
	}
	
	//增加一条服务器属性
	public function addServerAttr($data){
		return $this->db->table($this->tbl_attribute)->insert($data);
	}
	
	//删除一条服务器的ip
	public function delServerChildIpById($id){
		return $this->db->table($this->tbl_server_ip)->where('`pid`>0 AND id=?' , array($id))->delete();
	}
	
	//删除指定服务器ip的属性
	public function delServerAttrChildIPById($id){
		return $this->db->table($this->tbl_attribute)->where('`type`="server" AND `pid`=?' , array($id))->delete();
	}
	
	//删除服务器
	public function delServerByList($list){
		if (empty($list)){
			return false;
		}
		$ids = '';
		foreach ($list as $v){
			$ids .= $v['id'].',';
		}
		$ids = trim($ids, ',');
		//print_r($ids);exit();
		$sql_info = 'DELETE FROM '.$this->tbl_server_ip.' where id IN ('.$ids.')';
		$r = $this->db->query($sql_info);
		if ($r){
			$sql_attr = 'DELETE FROM '.$this->tbl_attribute.' where `type`="server" AND pid IN ('.$ids.')';
			return $this->db->query($sql_attr);
		}
		return false;
	}
	
	
	
}