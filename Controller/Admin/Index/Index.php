<?php
namespace Controller\Admin\Index;
class Index extends \Controller\Admin\Common\Common{

    public $versionLogService;

    public function __construct(){
        parent::__construct();
        $this->versionLogService = new \Service\Develop\VersionLogService();
    }

	public function index(){
        $layout_params = array(
            'header' => '',
            'footer' => '',
            'slide'  => array('index')
        );
        $this->setHeaderFooter($layout_params);
        $this->view->display('Admin/Index/index.html');

	}

    public function getLatestVersionLog(){
        $this->ret = $this->versionLogService->getLatestVersionLog();
        $this->ajaxReturn($this->ret);
    }
}
